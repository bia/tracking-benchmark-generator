package plugins.nchenouard.particleTracking.sequenceGenerator;

import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.TypeUtil;

import java.util.ArrayList;

import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Point3D;
import flanagan.math.PsRandom;

public class TrackGeneratorWithProfiles 
{
	public class HeavyBrownianAndSlightlyDirectedTarget extends Target
	{
		PsRandom ran = new PsRandom();
		double s = 2;
		Point3D vector = new Point3D();
		double length = 0;
		double step = 0;
		Point3D startPosition ;
		Point3D brownianCumul  = new Point3D(0,0,0);

		HeavyBrownianAndSlightlyDirectedTarget(Profile profile, Point3D startPosition , Point3D endPosition , double length ) {
			super(profile, startPosition );
			vector = new Point3D(
					( endPosition.x - startPosition.x ) / length , 
					( endPosition.y - startPosition.y ) / length , 
					( endPosition.z - startPosition.z ) / length  							
			);
			this.length = length ;
			this.startPosition = startPosition ; 
		}

		@Override
		Detection createDetection(int t)
		{
			return new ProfileSpotTrack(new ProfileSpot(super.profile, position), t);
		}

		@Override
		void move()
		{	
			step++;
			double distance = Math.sqrt( ( step - length / 2d ) * ( step - length / 2d ) );
			double coef = distance / (length /2d) ;
			coef = 1d -coef;
			//System.out.println( "step : " + step + " coef : " + coef );

			brownianCumul.x += ran.nextGaussian( 0, s );
			brownianCumul.y += ran.nextGaussian( 0, s );
			brownianCumul.z += ran.nextGaussian( 0, s ) /scaleZ	;

			position = new Point3D(
//					position.x + step * vector.x + ran.nextGaussian( 0, coef * s ) ,
//					position.y + step * vector.y + ran.nextGaussian( 0, coef * s ) ,
//					position.z + step * vector.z + ran.nextGaussian( 0, coef * s ) /scaleZ

//					startPosition.x + step * vector.x + ran.nextGaussian( 0, coef * s ) ,
//					startPosition.y + step * vector.y + ran.nextGaussian( 0, coef * s ) ,
//					startPosition.z + step * vector.z + ran.nextGaussian( 0, coef * s ) /scaleZ

					startPosition.x + step * vector.x + coef * brownianCumul.x ,
					startPosition.y + step * vector.y + coef * brownianCumul.y ,
					startPosition.z + step * vector.z + coef * brownianCumul.z 	
			);
		}
	}

	public class BrownianTarget extends Target
	{
		PsRandom ran = new PsRandom();

		double s;
		int dim;
		BrownianTarget(Profile profile, Point3D position, double s, int dim) {
			super(profile, position);
			this.s = s;
			this.dim = dim;
			ran.setSeed((long)( 1000000*Math.random()));
		}

		@Override
		Detection createDetection(int t)
		{
			return new ProfileSpotTrack(new ProfileSpot(super.profile, position), t);
		}

		@Override
		void move()
		{			
			if (this.dim==2)
				position = new Point3D(position.x + ran.nextGaussian(0, s), position.y + ran.nextGaussian(0, s), position.z);
			else
				position = new Point3D(position.x + ran.nextGaussian(0, s), position.y + ran.nextGaussian(0, s), position.z + ran.nextGaussian(0, s)/scaleZ);
		}
	}

	public class DirectedTarget extends Target
	{
		double s;
		double[] v;
		int dim;
		PsRandom ran = new PsRandom();

		DirectedTarget(Profile profile, Point3D position, double s, double[] v, int dim) {
			super(profile, position);
			this.s = s;
			this.v = v;
			this.dim = dim;
		}

		@Override
		Detection createDetection(int t)
		{
			return new ProfileSpotTrack(new ProfileSpot(super.profile, position), t);
		}
		@Override
		void move()
		{
			if (this.dim == 2)
				position = new Point3D(position.x + ran.nextGaussian(v[0], s), position.y + ran.nextGaussian(v[1], s), position.z);
			else
				position = new Point3D(position.x + ran.nextGaussian(v[0], s), position.y + ran.nextGaussian(v[1], s), position.z + ran.nextGaussian(v[2], s)/scaleZ);
		}
	}

	public class SwitchingMotionTypeTarget extends Target
	{
		double s;
		double sd;
		double v;
		int dim;
		double p_db;
		double p_bd;
		double[] vVect;
		int motionType;//0 for brownian, 1 for directed motion
		PsRandom ran = new PsRandom();
		boolean exists = true;
		double pDisappear;

		SwitchingMotionTypeTarget(Profile profile, Point3D position, double s, double sd, double v, int dim, double p_db, double p_bd, double pDisappear)
		{
			super(profile, position);
			this.s = s;
			this.sd = sd;
			this.v = v;
			this.dim = dim;
			this.p_bd = p_bd;
			this.p_db = p_db;
			double d = Math.random();
			this.pDisappear = pDisappear;
//			System.out.println("s sd"+s+" "+sd);
			if (d<(p_bd/(p_db+p_bd)))
			{
				reinitVelocity();
				motionType = 1;
			}
			else
				motionType = 0;
			ran.setSeed((long)(Math.random()*10000));
		}

		private void reinitVelocity()
		{
			if (this.dim==2)
			{
				vVect = new double[2];
				vVect[0] = Math.random()-0.5;
				vVect[1] = Math.random()-0.5;
				double n = Math.sqrt(vVect[0]*vVect[0] + vVect[1]*vVect[1]);
				vVect[0]*=(v/n);
				vVect[1]*=(v/n);
			}
			else
			{
				vVect = new double[3];
				vVect[0] = Math.random()-0.5;
				vVect[1] = Math.random()-0.5;
				vVect[2] = Math.random()-0.5;
				double n = Math.sqrt(vVect[0]*vVect[0] + vVect[1]*vVect[2]+ vVect[2]*vVect[2]);
				vVect[0]*=(v/n);
				vVect[1]*=(v/n);
				vVect[2]*=(v/n);
			}
		}

		@Override
		Detection createDetection(int t) {
			if (exists)
				return new ProfileSpotTrack(new ProfileSpot(super.profile, position), t);
			else
				return null;
		}

		@Override
		void move()
		{
			if (exists && Math.random()>pDisappear)
			{
				double d = Math.random();
				if(motionType == 0 && d<p_bd)
				{
					reinitVelocity();
					motionType = 1;
				}
				else if(motionType == 1 && d<p_db)
					motionType = 0;
				if (motionType==0)
				{
					if (this.dim==2)
						position = new Point3D(position.x + ran.nextGaussian(0, s), position.y + ran.nextGaussian(0, s), position.z);
					else
						position = new Point3D(position.x + ran.nextGaussian(0, s), position.y + ran.nextGaussian(0, s), position.z + ran.nextGaussian(0, s)/scaleZ);

				}
				else
				{
					if (this.dim == 2)
						position = new Point3D(position.x + ran.nextGaussian(vVect[0], sd), position.y + ran.nextGaussian(vVect[1], sd), position.z);
					else
						position = new Point3D(position.x + ran.nextGaussian(vVect[0], sd), position.y + ran.nextGaussian(vVect[1], sd), position.z + ran.nextGaussian(vVect[2], sd)/scaleZ);
				}
			}
			else
				exists = false;
		}

		@Override
		boolean exists()
		{
			return exists;
		}

	}

	public class Monom
	{
		final public double[] coeff;
		final public double degree;

		Monom(double[] coeff, double degree)
		{
			this.coeff = coeff;
			this.degree = degree;
		}

		double[] getValues(double t)
		{
			double[] values = new double[coeff.length];
			for (int i = 0; i < coeff.length; i++)
				values[i] = coeff[i]*Math.pow(t, degree);
			return values;
		}
	}

	public abstract class Target
	{
		Point3D position;
		Profile profile;

		Target(Profile profile, Point3D position)
		{
			this.profile = profile;
			this.position = position;
		}

		boolean exists()
		{
			return true;
		}

		abstract Detection createDetection(int t);
		abstract void move();
	}

	int dim = 3;
	int[] dims = new int[]{256, 256, 40};
	double scaleZ = 3;

	public TrackGeneratorWithProfiles(){}

	public TrackGeneratorWithProfiles(int dim, int[] dims, double scaleZ)
	{
		this.dims = dims;
		this.dim = dim;
		this.scaleZ = scaleZ;
	}

	public static ArrayList<Profile> createCLSM05Profiles_2d(int numProfiles, double minAmplitude, double maxAmplitude, double minZ, double maxZ) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			double z = minZ + Math.random()*(maxZ - minZ);
			profiles.add(PSF_2D.autoLoadDefocalizedPSF_CLSMNA05(amplitude, 1, 3, z));
		}
		return profiles;
	}

	public static ArrayList<Profile> createCLSM05Profiles_3d(int numProfiles, double minAmplitude, double maxAmplitude, double scaleZ) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			profiles.add(PSF_3D.autoLoadPSF_CLSMNA05(amplitude, 1, scaleZ));
		}
		return profiles;
	}

	public ArrayList<Profile> createCLSM05Profiles_3d(int numProfiles, double minAmplitude, double maxAmplitude) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			profiles.add(PSF_3D.autoLoadPSF_CLSMNA05(amplitude, 1, scaleZ));
		}
		return profiles;
	}

	public static ArrayList<Profile> createCLSM1Profiles_2d(int numProfiles, double minAmplitude, double maxAmplitude, double minZ, double maxZ) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			double z = minZ + Math.random()*(maxZ - minZ);
			profiles.add(PSF_2D.autoLoadDefocalizedPSF_CLSMNA1(amplitude, 1, 3, z));
		}
		return profiles;
	}

	public static ArrayList<Profile> createCLSM1Profiles_3d(int numProfiles, double minAmplitude, double maxAmplitude, double scaleZ) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			profiles.add(PSF_3D.autoLoadPSF_CLSMNA1(amplitude, 1, scaleZ));
		}
		return profiles;
	}

	public ArrayList<Profile> createCLSM1Profiles_3d(int numProfiles, double minAmplitude, double maxAmplitude) {
		ArrayList<Profile> profiles = new ArrayList<Profile>(numProfiles);
		for (int i = 0; i < numProfiles; i++)
		{
			double amplitude = minAmplitude + Math.random()*(maxAmplitude - minAmplitude);
			profiles.add(PSF_3D.autoLoadPSF_CLSMNA1(amplitude, 1, scaleZ));
		}
		return profiles;
	}

	public double[] createRandomDiffusion(int numProfiles, double mean, double std) {
		PsRandom ran = new PsRandom();
		double[] s = ran.gaussianArray(mean, std, numProfiles);
		return s;
	}

	public static TrackSegment createTrack(Target target, int firstT, int lastT)
	{
		TrackSegment ts = new TrackSegment();
		for (int t=firstT; t <= lastT; t++)
		{
			//for diffusion changes estimation
			//		if (t==50)
			//			((BrownianTarget)target).s = 6;
			if (target.exists())
			{
				ts.addDetection(target.createDetection(t));
				target.move();
			}
		}
		return ts;
	}
//	public Sequence createTracksImage(ArrayList<TrackSegment> ts, ArrayList<Profile> profiles, int trackLength, int[] dims, int dim, double backMean, double noiseSig, boolean type8bit, String title)
//	{
//		return createTracksImageGaussian(ts, profiles, trackLength, dims, dim, scaleZ, backMean, noiseSig, type8bit, title);
//	}
	
	public static Sequence createTracksImageGaussian(ArrayList<TrackSegment> ts, ArrayList<Profile> profiles, int trackLength, int[] dims, int dim, double scaleZ, double backMean, double noiseSig, int dataType, String title)
	{
		Sequence newSeq = new Sequence();
		PsRandom ran = new PsRandom();
		int numTargets = ts.size();
		if (profiles.size() != numTargets)
			return newSeq;
		if (dim == 2)
		{
			ArrayList<double[]> arrays = new ArrayList<double[]>(trackLength);
			for (int t = 0; t < trackLength; t++)
			{
				arrays.add(ran.gaussianArray(backMean, noiseSig, dims[0]*dims[1]));
			}	
			int[] maxDist = new int []{20,20};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();
				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[] array = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);

					for (int y = minY; y<= maxY; y++)
						for (int x = minX; x<= maxX; x++)
							array[x+y*dims[0]] = array[x+y*dims[0]] + p.getValue(new double[]{x, y, d.getX(), d.getY()});
				}
				cnt++;
			}
			for (int t =0; t < arrays.size(); t++)
			{
				IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
				img.setDataXYAsDouble(0, arrays.get(t));
				img = img.convertToType(dataType, false, false);
				img.dataChanged();
				newSeq.setImage(t, 0, img);
			}
		}
		else
		{
			ArrayList<double[][]> arrays = new ArrayList<double[][]>(trackLength);
			for (int t = 0; t < trackLength; t++)
			{
				double[][] volume = new double[dims[2]][];
				for (int z = 0; z < dims[2]; z++)
					volume[z] = ran.gaussianArray(backMean, noiseSig, dims[0]*dims[1]);
				arrays.add(volume);
			}
			int[] maxDist = new int []{20,20, 10};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();

				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[][] volume = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int minZ = Math.max((int)(d.getZ() - maxDist[2]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);
					int maxZ = Math.min((int)(d.getZ() + maxDist[2]), dims[2]-1);
					for (int z = minZ; z <=maxZ; z++)
						for (int y = minY; y<= maxY; y++)
							for (int x = minX; x<= maxX; x++)
								volume[z][x+y*dims[0]] = volume[z][x+y*dims[0]] + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
				}
				cnt++;
			}
			for (int t =0; t < arrays.size(); t++)
			{
				//newSeq.add(ImageToTabUtils.grayTabToVI(arrays.get(t), dims[0], dims[1], dims[2], type8bit, false));
				double[][] volume = arrays.get(t);
				for (int z = 0; z < dims[2]; z++)
				{
					IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
					img.setDataXYAsDouble(0, volume[z]);
					img = img.convertToType(dataType, false, false);
					img.dataChanged();
					newSeq.setImage(t, z, img);
				}
			}
		}
		return newSeq;
	}
	
	public static Sequence createTracksImagePoissonAndGaussian(ArrayList<TrackSegment> ts, ArrayList<Profile> profiles, int trackLength, int[] dims, int dim, double scaleZ, double gaussianMean, double noiseSig, double gain, double darkNoise, int dataType, String title)
	{
		Sequence newSeq = new Sequence();
		PsRandom ran = new PsRandom();
		int numTargets = ts.size();
		if (profiles.size() != numTargets)
			return newSeq;
		if (dim == 2)
		{
			ArrayList<double[]> arrays = new ArrayList<double[]>(trackLength);
			for (int t =0; t < trackLength; t++)
				arrays.add(new double[dims[0]*dims[1]]);
			int[] maxDist = new int []{20,20};

			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();
				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[] array = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);

					for (int y = minY; y<= maxY; y++)
						for (int x = minX; x<= maxX; x++)
							array[x+y*dims[0]] = array[x+y*dims[0]] + p.getValue(new double[]{x, y, d.getX(), d.getY()});
				}
				cnt++;
			}
			for(double[] array:arrays)
				for(int i =0; i < array.length; i++)
				{
					array[i] = ran.nextGaussian(gaussianMean, noiseSig) + gain*(ran.nextPoissonian(array[i]) + ran.nextPoissonian(darkNoise));
				}
			for(int t = 0; t<arrays.size(); t++)
			{
				IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
				img.setDataXYAsDouble(0, arrays.get(t));
				img = img.convertToType(dataType, false, false);
				img.dataChanged();
				newSeq.setImage(t, 0, img);
				//newSeq.add(ImageToTabUtils.grayTabToVI(array, dims[0], dims[1], 1, type8bit, false));
			}
		}
		else
		{
			ArrayList<double[][]> arrays = new ArrayList<double[][]>(trackLength);
			for (int t =0; t < trackLength; t++)
				arrays.add(new double[dims[2]][dims[0]*dims[1]]);
			int[] maxDist = new int []{20,20, 10};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();

				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[][] volume = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int minZ = Math.max((int)(d.getZ() - maxDist[2]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);
					int maxZ = Math.min((int)(d.getZ() + maxDist[2]), dims[2]-1);
					for (int z = minZ; z <=maxZ; z++)
						for (int y = minY; y<= maxY; y++)
							for (int x = minX; x<= maxX; x++)
								volume[z][x+y*dims[0]] = volume[z][x+y*dims[0]] +  + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
					//array[x+y*dims[0]+z*dims[0]*dims[1]] = array[x+y*dims[0]+z*dims[0]*dims[1]] + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
				}
				cnt++;
			}
			for(double[][] volume:arrays)
				for(int z =0; z < volume.length; z++)
				{
					double[] array = volume[z];
					for (int i = 0; i < array.length; i++)
						array[i] = ran.nextGaussian(gaussianMean, noiseSig) + gain*(ran.nextPoissonian(array[i]) + ran.nextPoissonian(darkNoise));
				}
			//for(double[] array:arrays)			
			//	newSeq.add(ImageToTabUtils.grayTabToVI(array, dims[0], dims[1], dims[2], type8bit, false));
			for (int t = 0; t < arrays.size(); t++)
			{
				double[][] volume = arrays.get(t);
				for (int z = 0; z < dims[2]; z++)
				{
					IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
					img.setDataXYAsDouble(0, volume[z]);
					img = img.convertToType(dataType, false, false);
					img.dataChanged();
					newSeq.setImage(t, z, img);
				}
			}
		}
		return newSeq;
	}

	public static Sequence createTracksImagePoisson(ArrayList<TrackSegment> ts, ArrayList<Profile> profiles, int trackLength, int[] dims, int dim, double scaleZ, double gain, double darkNoise, int dataType, String title)
	{
		Sequence newSeq = new Sequence();
		PsRandom ran = new PsRandom();
		int numTargets = ts.size();
		if (profiles.size() != numTargets)
			return newSeq;
		if (dim == 2)
		{
			ArrayList<double[]> arrays = new ArrayList<double[]>(trackLength);
			for (int t =0; t < trackLength; t++)
				arrays.add(new double[dims[0]*dims[1]]);
			int[] maxDist = new int []{20,20};

			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();
				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[] array = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);

					for (int y = minY; y<= maxY; y++)
						for (int x = minX; x<= maxX; x++)
							array[x+y*dims[0]] = array[x+y*dims[0]] + p.getValue(new double[]{x, y, d.getX(), d.getY()});
				}
				cnt++;
			}
			for(double[] array:arrays)
				for(int i =0; i < array.length; i++)
				{
					array[i] = gain*(ran.nextPoissonian(array[i]) + ran.nextPoissonian(darkNoise));
				}
			for(int t = 0; t<arrays.size(); t++)
			{
				IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
				img.setDataXYAsDouble(0, arrays.get(t));
				img = img.convertToType(dataType, false, false);
				img.dataChanged();
				newSeq.setImage(t, 0, img);
				//newSeq.add(ImageToTabUtils.grayTabToVI(array, dims[0], dims[1], 1, type8bit, false));
			}
		}
		else
		{
			ArrayList<double[][]> arrays = new ArrayList<double[][]>(trackLength);
			for (int t =0; t < trackLength; t++)
				arrays.add(new double[dims[2]][dims[0]*dims[1]]);
			int[] maxDist = new int []{20,20, 10};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();

				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[][] volume = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int minZ = Math.max((int)(d.getZ() - maxDist[2]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);
					int maxZ = Math.min((int)(d.getZ() + maxDist[2]), dims[2]-1);
					for (int z = minZ; z <=maxZ; z++)
						for (int y = minY; y<= maxY; y++)
							for (int x = minX; x<= maxX; x++)
								volume[z][x+y*dims[0]] = volume[z][x+y*dims[0]] +  + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
					//array[x+y*dims[0]+z*dims[0]*dims[1]] = array[x+y*dims[0]+z*dims[0]*dims[1]] + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
				}
				cnt++;
			}
			for(double[][] volume:arrays)
				for(int z =0; z < volume.length; z++)
				{
					double[] array = volume[z];
					for (int i = 0; i < array.length; i++)
						array[i] = gain*(ran.nextPoissonian(array[i]) + ran.nextPoissonian(darkNoise));
				}
			//for(double[] array:arrays)			
			//	newSeq.add(ImageToTabUtils.grayTabToVI(array, dims[0], dims[1], dims[2], type8bit, false));
			for (int t = 0; t < arrays.size(); t++)
			{
				double[][] volume = arrays.get(t);
				for (int z = 0; z < dims[2]; z++)
				{
					IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
					img.setDataXYAsDouble(0, volume[z]);
					img = img.convertToType(dataType, false, false);
					img.dataChanged();
					newSeq.setImage(t, z, img);
				}
			}
		}
		return newSeq;
	}

	public static Sequence createTracksImage(ArrayList<TrackSegment> ts, ArrayList<Profile> profiles, int trackLength, int[] dims, int dim, double scaleZ, int dataType, String title)
	{
		Sequence newSeq = new Sequence();
		//PsRandom ran = new PsRandom();
		int numTargets = ts.size();
		if (profiles.size() != numTargets)
			return newSeq;
		if (dim == 2)
		{
			ArrayList<double[]> arrays = new ArrayList<double[]>(trackLength);
			for (int t = 0; t < trackLength; t++)
			{
				arrays.add(new double[dims[0]*dims[1]]);
			}	
			int[] maxDist = new int []{20,20};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();
				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[] array = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);

					for (int y = minY; y<= maxY; y++)
						for (int x = minX; x<= maxX; x++)
							array[x+y*dims[0]] = array[x+y*dims[0]] + p.getValue(new double[]{x, y, d.getX(), d.getY()});
				}
				cnt++;
			}
			for (int t =0; t < arrays.size(); t++)
			{
				IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
				img.setDataXYAsDouble(0, arrays.get(t));
				img = img.convertToType(dataType, false, false);
				img.dataChanged();
				newSeq.setImage(t, 0, img);
				//newSeq.add(ImageToTabUtils.grayTabToVI(arrays.get(t), dims[0], dims[1], 1, type8bit, false));
			}
		}
		else
		{
			ArrayList<double[][]> arrays = new ArrayList<double[][]>(trackLength);
			for (int t = 0; t < trackLength; t++)
			{
				double[][] volume = new double[dims[2]][];
				for (int z = 0; z < dims[2]; z++)
					volume[z] = new double[dims[0]*dims[1]];
				arrays.add(volume);
			}
			int[] maxDist = new int []{20,20, 10};
			int cnt = 0;
			for (TrackSegment track:ts)
			{
				int firstT = track.getFirstDetection().getT();
				int lastT = track.getLastDetection().getT();

				Profile p = profiles.get(cnt);
				for(int i = 0; i < lastT-firstT+1; i++)
				{
					double[][] volume = arrays.get(firstT+i);
					Detection d = track.getDetectionAt(i);
					int minX = Math.max((int)(d.getX() - maxDist[0]), 0);
					int minY = Math.max((int)(d.getY() - maxDist[1]), 0);
					int minZ = Math.max((int)(d.getZ() - maxDist[2]), 0);
					int maxX = Math.min((int)(d.getX() + maxDist[0]), dims[0]-1);
					int maxY = Math.min((int)(d.getY() + maxDist[1]), dims[1]-1);
					int maxZ = Math.min((int)(d.getZ() + maxDist[2]), dims[2]-1);
					for (int z = minZ; z <=maxZ; z++)
						for (int y = minY; y<= maxY; y++)
							for (int x = minX; x<= maxX; x++)
								volume[z][x+y*dims[0]] = volume[z][x+y*dims[0]] + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
					//array[x+y*dims[0]+z*dims[0]*dims[1]] = array[x+y*dims[0]+z*dims[0]*dims[1]] + p.getValue(new double[]{x, y, z*scaleZ, d.getX(), d.getY(), scaleZ*d.getZ()});
				}
				cnt++;
			}
			for (int t =0; t < arrays.size(); t++)
			{
				//newSeq.add(ImageToTabUtils.grayTabToVI(arrays.get(t), dims[0], dims[1], dims[2], type8bit, false));
				double[][] volume = arrays.get(t);
				for (int z = 0; z < dims[2]; z++)
				{
					IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
					img.setDataXYAsDouble(0, volume[z]);
					img = img.convertToType(dataType, false, false);
					img.dataChanged();
					newSeq.setImage(t, z, img);
				}
			}
		}
		return newSeq;
	}
	
	public static Sequence createEmptyImage(int trackLength, int[] dims, int dim, double backMean, double noiseSig, int dataType, String title)
	{
		Sequence newSeq = new Sequence();
		PsRandom ran = new PsRandom();
		if (dim == 2)
		{
			for (int t = 0; t < trackLength; t++)
			{
				IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
				img.setDataXYAsDouble(0, ran.gaussianArray(backMean, noiseSig, dims[0]*dims[1]));
				img = img.convertToType(dataType, false, false);
				img.dataChanged();
				newSeq.setImage(t, 0, img);
				
			}
		}
		else
		{
			for (int t = 0; t < trackLength; t++)
			{
				for (int z = 0; z < dims[2]; z++)
				{
					IcyBufferedImage img = new IcyBufferedImage(dims[0], dims[1], 1, TypeUtil.TYPE_DOUBLE);
					img.setDataXYAsDouble(0, ran.gaussianArray(backMean, noiseSig, dims[0]*dims[1]));
					img = img.convertToType(dataType, false, false);
					img.dataChanged();
					newSeq.setImage(t, z, img);
				}
			}
		}
		return newSeq;
	}

	public BrownianTarget createBrownianTarget(Profile profile, Point3D position, double s, int dim)
	{
		return new BrownianTarget(profile, position, s, dim);
	}

	public DirectedTarget createDirectedTarget(Profile profile, Point3D position, double s, double[] v, int dim)
	{
		DirectedTarget target = new DirectedTarget(profile, position, s, v, dim);
		return target;		
	}

	public HeavyBrownianAndSlightlyDirectedTarget createHeavyBrownianAndSlightlyDirectedTarget(Profile profile, Point3D startPosition , Point3D endPosition , double length )
	{
		return new HeavyBrownianAndSlightlyDirectedTarget(profile, startPosition , endPosition , length);
	}

	public SwitchingMotionTypeTarget createSwitchingMotionTypeTarget(Profile profile, Point3D position, double s, double sd, double v, int dim, double p_db, double p_bd, double pDisappear)
	{
		SwitchingMotionTypeTarget target = new SwitchingMotionTypeTarget(profile, position, s, sd, v, dim, p_db, p_bd, pDisappear);
		return target;		
	}
}
