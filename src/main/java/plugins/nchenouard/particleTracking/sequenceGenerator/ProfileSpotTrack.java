package plugins.nchenouard.particleTracking.sequenceGenerator;

import icy.canvas.IcyCanvas;
import icy.sequence.Sequence;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

import plugins.nchenouard.particletracking.legacytracker.SpotTrack;

public class ProfileSpotTrack extends SpotTrack
{
    public ProfileSpot spot;
    static boolean drawCenter = true;
    static boolean drawLevels = true;

    public ProfileSpotTrack()
    {
        super();

        spot = null;
    }

    public ProfileSpotTrack(ProfileSpot spot, int t)
    {
        super(spot, t, true, 0, 0, 0, null);
        // super(spot, t, true);
        this.spot = spot;
    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas quiaCanvas)
    {
        if (enabled)
        {
            if (g instanceof Graphics2D)
            {
                Graphics2D g2d = (Graphics2D) g;

                if (drawLevels && (spot != null))
                {
                    g2d.setStroke(new BasicStroke(0.2f));

                    if (spot.profile instanceof GaussProfile2DPF)
                    {
                        GaussProfile2D p = ((GaussProfile2DPF) spot.profile).expectedProfile;
                        double stdx = Math.sqrt(p.getVarx());
                        double stdy = Math.sqrt(p.getVary());
                        int k = 3;
                        g2d.setColor(Color.orange);
                        Ellipse2D e = new Ellipse2D.Double(x - k * stdx / 2, y - k * stdy / 2, k * stdx, k * stdy);
                        g2d.draw(e);
                    }
                    if (spot.profile instanceof GaussProfile2D)
                    {
                        GaussProfile2D p = (GaussProfile2D) spot.profile;
                        double stdx = Math.sqrt(p.getVarx());
                        double stdy = Math.sqrt(p.getVary());
                        int k = 3;
                        g2d.setColor(Color.GREEN);
                        Ellipse2D e = new Ellipse2D.Double(x - k * stdx / 2, y - k * stdy / 2, k * stdx, k * stdy);
                        g2d.draw(e);
                        g2d.setColor(Color.orange);
                        // k = 2;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                        // g2d.setColor(Color.yellow);
                        // k = 5;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                    }
                    else if (spot.profile instanceof GaussProfile3D)
                    {
                        g2d.setColor(getColor());
                        GaussProfile3D p = (GaussProfile3D) spot.profile;
                        double stdx = Math.sqrt(p.getVarx());
                        double stdy = Math.sqrt(p.getVary());
                        int k = 3;
                        g2d.setColor(getColor());
                        Ellipse2D e = new Ellipse2D.Double(x - k * stdx / 2, y - k * stdy / 2, k * stdx, k * stdy);
                        g2d.draw(e);
                        // g2d.setColor(Color.orange);
                        // k = 2;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                        // g2d.setColor(Color.yellow);
                        // k = 5;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                    }
                    else if (spot.profile instanceof SimpleGaussProfile2D)
                    {
                        SimpleGaussProfile2D p = (SimpleGaussProfile2D) spot.profile;
                        double stdxy = Math.sqrt(p.getVarXY());
                        int k = 3;
                        g2d.setColor(Color.GREEN);
                        Ellipse2D e = new Ellipse2D.Double(x - k * stdxy / 2, y - k * stdxy / 2, k * stdxy, k * stdxy);
                        g2d.draw(e);
                        // g2d.setColor(Color.orange);
                        // k = 2;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                        // g2d.setColor(Color.yellow);
                        // k = 5;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                    }
                    else if (spot.profile instanceof SimpleGaussProfile3D)
                    {
                        g2d.setColor(getColor());
                        SimpleGaussProfile3D p = (SimpleGaussProfile3D) spot.profile;
                        double stdxy = Math.sqrt(p.getVarXY());
                        int k = 3;
                        g2d.setColor(getColor());
                        Ellipse2D e = new Ellipse2D.Double(x - k * stdxy / 2, y - k * stdxy / 2, k * stdxy, k * stdxy);
                        g2d.draw(e);
                        // g2d.setColor(Color.orange);
                        // k = 2;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                        // g2d.setColor(Color.yellow);
                        // k = 5;
                        // e = new Ellipse2D.Double(x - k*stdx/2, y - k*stdy/2, k*stdx, k*stdy);
                        // g2d.draw(e);
                    }
                    if (spot.profile instanceof RotatedGaussProfile2D)
                    {
                        RotatedGaussProfile2D p = (RotatedGaussProfile2D) spot.profile;
                        double stdx = Math.sqrt(p.getVarx());
                        double stdy = Math.sqrt(p.getVary());
                        int k = 3;
                        g2d.setColor(Color.GREEN);
                        double theta = p.getTheta();
                        g2d.translate(x, y);
                        g2d.rotate(theta);
                        Ellipse2D e = new Ellipse2D.Double((-k * stdx / 2), (-k * stdy / 2), (k * stdx), (k * stdy));
                        g2d.draw(e);
                        g2d.rotate(-theta);
                        g2d.translate(-x, -y);
                        g2d.setColor(Color.orange);

                    }
                    g2d.setStroke(new BasicStroke(1f));
                }
                if (drawCenter)
                {
                    g2d.setColor(getColor());
                    g2d.setStroke(new BasicStroke(0.2f));
                    Line2D line;
                    line = new Line2D.Double(x - 1, y, x + 1, y);
                    g2d.draw(line);
                    line = new Line2D.Double(x, y - 1, x, y + 1);
                    g2d.draw(line);
                    g2d.setStroke(new BasicStroke(1f));
                }
            }
            else
            // TODO: 3D painting of the profile
            {
                super.paint(g, sequence, quiaCanvas);
                // g.setColor(super.color);
                // if (spot.profile instanceof GaussProfile3D)
                // {
                // GaussProfile3D p = (GaussProfile3D)spot.profile;
                // double stdx = Math.sqrt(p.getVarx());
                // double stdy = Math.sqrt(p.getVary());
                // double stdz = Math.sqrt(p.getVarz());
                // int k = 2;
                // g.drawEllipse((float)x, (float)y,(float)z, (float)(k*stdx/2), (float)(k*stdy/2), (float)(k*stdz/2),
                // 10, 10, true );
                // }
                // else if (spot.profile instanceof SimpleGaussProfile3D)
                // {
                // SimpleGaussProfile3D p = (SimpleGaussProfile3D)spot.profile;
                // double stdxy = Math.sqrt(p.getVarXY());
                // double stdz = Math.sqrt(p.getVarZ());
                // float k = 2;
                // g.drawEllipse((float)x, (float)y,(float)z, (float)(k*stdxy/2), (float)(k*stdxy/2), (float)(k*stdz/2),
                // 10, 10, true );

                // }
                // if(drawCenter)
                // {
                // g.drawBox((float)(x-0.25), (float)(y-0.25), (float)(z-0.25), 0.5f, 0.5f, 0.5f);
                // }

            }
        }
    }
}
