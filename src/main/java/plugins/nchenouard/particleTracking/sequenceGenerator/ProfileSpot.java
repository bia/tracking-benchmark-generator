package plugins.nchenouard.particleTracking.sequenceGenerator;

import plugins.nchenouard.spot.Point3D;
import plugins.nchenouard.spot.Spot;

public class ProfileSpot extends Spot
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2305996179584619721L;
	
	public Profile profile;
	
	public ProfileSpot(Profile profile, Point3D center)
	{
		super();
		super.mass_center = center;
		this.profile = profile;
	}
	


}
