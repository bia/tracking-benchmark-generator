package plugins.nchenouard.particleTracking.sequenceGenerator;

import icy.type.TypeUtil;

public class Bench2DConfiguration
{
	// benchmark configuration
	private int numBenchs = 1;
	private String saveDir = "/Users/me/Desktop/benchmark";
	private boolean displaySequences = true;
	private boolean saveSequences = false;

	// sequence configuration
	private final static int dim = 2;
	private int seqLength = 25;
	private int width = 256;
	private int height = 256;
	private int dataType = TypeUtil.TYPE_DOUBLE;
	
	// particles configuration
	private int numParticleGroups = 1; 	// number of particle groups
	private double[] numNewParticlesPerFrame = new double[]{1.0};
	private int[] numInitParticles = new int[]{20};
	private double pDisappear = 0.05;
	private double[] sigma_bMax = {4};
	private double[] sigma_bMin = {2};
	private double[] sigma_directedMin = new double[]{1.0};
	private double[] sigma_directedMax = new double[]{2.0};
	private double[] vMin = new double[]{2};
	private double[] vMax = new double[]{4};
	
	private double[] p_bd = new double[]{0.2};//switch from Brownian to directed motion
	private double[] p_db = new double[]{0.2};//switch from directed to Brownian motion
	private double[] iMin = new double[]{20};
	private double[] iMax = new double[]{25};
	
	// Poisson noise settings
	private boolean PoissonNoise = false;
	private double backgroundPoisson = 15;
	private double gain = 1;
	
	// Gaussian noise settings
	private boolean GaussianNoise = true;	
	private double meanGaussian = 50;
	private double stdGaussian = 5;

	public int getNumBenchs() {
		return numBenchs;
	}

	public void setNumBenchs(int numBenchs) throws IllegalArgumentException{
		if (numBenchs >0)
			this.numBenchs = numBenchs;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("numBenchs has to be a strictly positive integer"));
	}

	public String getSaveDir() {
		return saveDir;
	}

	public void setSaveDir(String saveDir) {
		this.saveDir = saveDir.toString();
	}

	public boolean isDisplaySequences() {
		return displaySequences;
	}

	public void setDisplaySequences(boolean displaySequences) {
		this.displaySequences = displaySequences;
	}

	public boolean isSaveSequences() {
		return saveSequences;
	}

	public void setSaveSequences(boolean saveSequences) {
		this.saveSequences = saveSequences;
	}

	public int getSeqLength() {
		return seqLength;
	}

	public void setSeqLength(int seqLength) throws IllegalArgumentException{
		if (seqLength>0)
			this.seqLength = seqLength;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("seqLength has to be a strictly positive integer"));
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) throws IllegalArgumentException{
		if (width >0)
			this.width = width;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("width has to be a strictly positive integer"));
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) throws IllegalArgumentException{
		if (height > 0)
			this.height = height;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("height has to be a strictly positive integer"));
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) throws IllegalArgumentException{
		if (!TypeUtil.toString(dataType).equals("undefined"))
			this.dataType = dataType;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("Unkwnown image Type. Use a format defined in TypeUtil"));
	}

	public int getNumParticleGroups() {
		return numParticleGroups;
	}

	public void setNumParticleGroups(int numParticleGroups) throws IllegalArgumentException {
		if (numParticleGroups > 0)
			this.numParticleGroups = numParticleGroups;
		else
			throw new IllegalArgumentException(new IllegalArgumentException("numParticleGroups has to be a strictly positive integer"));
	}

	public double[] getNumNewParticlesPerFrame() {
		return numNewParticlesPerFrame.clone();
	}

	public void setNumNewParticlesPerFrame(double[] numNewParticlesPerFrame) throws IllegalArgumentException{
		for (double d:numNewParticlesPerFrame)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("numNewParticlesPerFrame has to be a table of strictly positive double"));
		this.numNewParticlesPerFrame = numNewParticlesPerFrame.clone();
	}

	public int[] getNumInitParticles() {
		return numInitParticles.clone();
	}

	public void setNumInitParticles(int[] numInitParticles) throws IllegalArgumentException{
		for (int d:numInitParticles)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("numInitParticles has to be a table of strictly positive integer"));		
		this.numInitParticles = numInitParticles.clone();
	}

	public double getpDisappear() {
		return pDisappear;
	}

	public void setpDisappear(double pDisappear) throws IllegalArgumentException{
		if (pDisappear>1 || pDisappear <0)
			throw new IllegalArgumentException(new IllegalArgumentException("pDisappear has to be a table of double between 0 and 1"));
		this.pDisappear = pDisappear;
	}

	public double[] getSigma_bMax() {
		return sigma_bMax.clone();
	}

	public void setSigma_bMax(double[] sigma_bMax) throws IllegalArgumentException{
		for (double d:sigma_bMax)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("sigma_bMax has to be a table of strictly positive double"));
		this.sigma_bMax = sigma_bMax.clone();
	}

	public double[] getSigma_bMin() {
		return sigma_bMin.clone();
	}

	public void setSigma_bMin(double[] sigma_bMin) throws IllegalArgumentException{
		for (double d:sigma_bMin)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("sigma_bMin has to be a table of strictly positive double"));		
		this.sigma_bMin = sigma_bMin.clone();
	}

	public double[] getSigma_directedMin() {
		return sigma_directedMin.clone();
	}
	
	public void setSigma_directedMin(double[] sigma_directedMin) throws IllegalArgumentException{
		for (double d:sigma_directedMin)
		if (d<0)
			throw new IllegalArgumentException(new IllegalArgumentException("sigma_directedMin has to be a table of strictly positive double"));
		this.sigma_directedMin = sigma_directedMin.clone();
	}

	public double[] getSigma_directedMax() {
		return sigma_directedMax.clone();
	}
	
	public void setSigma_directedMax(double[] sigma_directedMax) throws IllegalArgumentException{
		for (double d:sigma_directedMax)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("sigma_directedMax has to be a table of strictly positive double"));
		this.sigma_directedMax = sigma_directedMax.clone();
	}
	
	public double[] getVMin() {
		return vMin.clone();
	}

	public double[] getVMax() {
		return vMax.clone();
	}
	
	public void setVMin(double[] vMin) throws IllegalArgumentException{
		for (double d:vMin)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("vMin has to be a table of strictly positive double"));
		this.vMin = vMin.clone();
	}
	
	public void setVMax(double[] vMax) throws IllegalArgumentException{
		for (double d:vMax)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("vMax has to be a table of strictly positive double"));
		this.vMax = vMax.clone();
	}
	
	public double[] getP_bd() {
		return p_bd.clone();
	}

	public void setP_bd(double[] p_bd) throws IllegalArgumentException {
		for (double d:p_bd)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("p_bd has to be a table of strictly positive double"));
		this.p_bd = p_bd.clone();
	}

	public double[] getP_db() {
		return p_db.clone();
	}

	public void setP_db(double[] p_db) throws IllegalArgumentException {
		for (double d:p_db)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("p_db has to be a table of strictly positive double"));
		this.p_db = p_db;
	}

	public double[] getiMin() {
		return iMin.clone();
	}

	public void setiMin(double[] iMin) throws IllegalArgumentException {
		for (double d:iMin)
			if (d<0)
				throw new IllegalArgumentException(new IllegalArgumentException("iMin has to be a table of strictly positive double"));
		this.iMin = iMin;
	}

	public double[] getiMax() {
		return iMax.clone();
	}

	public void setiMax(double[] iMax) throws IllegalArgumentException {
		for (double d:iMax)
		if (d<0)
			throw new IllegalArgumentException(new IllegalArgumentException("iMax has to be a table of strictly positive double"));
		this.iMax = iMax.clone();
	}

	public boolean isPoissonNoise() {
		return PoissonNoise;
	}

	public void setPoissonNoise(boolean poissonNoise) {
		PoissonNoise = poissonNoise;
	}

	public double getBackgroundPoisson() {
		return backgroundPoisson;
	}

	public void setBackgroundPoisson(double backgroundPoisson) throws IllegalArgumentException {
		if (backgroundPoisson<0)
			throw new IllegalArgumentException(new IllegalArgumentException("backgroundPoisson has to be a table of strictly positive double"));
		this.backgroundPoisson = backgroundPoisson;
	}

	public double getGain() {
		return gain;
	}

	public void setGain(double gain) throws IllegalArgumentException {
		if (gain<0)
			throw new IllegalArgumentException(new IllegalArgumentException("gain has to be a table of strictly positive double"));
		this.gain = gain;
	}

	public boolean isGaussianNoise() {
		return GaussianNoise;
	}

	public void setGaussianNoise(boolean gaussianNoise) {
		GaussianNoise = gaussianNoise;
	}

	public double getMeanGaussian() {
		return meanGaussian;
	}

	public void setMeanGaussian(double meanGaussian) {
		this.meanGaussian = meanGaussian;
	}

	public double getStdGaussian() {
		return stdGaussian;
	}

	public void setStdGaussian(double stdGaussian) throws IllegalArgumentException {
		if (stdGaussian<0)
			throw new IllegalArgumentException(new IllegalArgumentException("stdGaussian has to be a table of strictly positive double"));
		this.stdGaussian = stdGaussian;
	}

	public String toString()
	{
		String str = new String();

		// benchmark configuration
		str = str.concat("numBenchs "+numBenchs+"\n");
		str = str.concat("saveDir "+saveDir+"\n");
		str = str.concat("saveSequences "+saveSequences+"\n");
		str = str.concat("displaySequences "+displaySequences+"\n");
		str = str.concat("\n");
		
		// sequence configuration
		str = str.concat("dim "+dim+"\n");
		str = str.concat("seqLength "+seqLength+"\n");
		str = str.concat("width "+width+"\n");
		str = str.concat("height "+width+"\n");
		str = str.concat("imageType " + dataTypeToString(dataType)+"\n");
		str = str.concat("\n");
		
		// particles configuration
		str = str.concat("numParticleGroups "+numParticleGroups+"\n");
		str = str.concat("numInitParticles");
		for (int d:numInitParticles)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("numNewParticlesPerFrame");
		for (double d:numNewParticlesPerFrame)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("pDisappear "+pDisappear+"\n");
		str = str.concat("sigma_bMax");
		for (double d:sigma_bMax)
			str = str.concat(" "+d);
		str = str.concat("\n");	
		str = str.concat("sigma_bMin");
		for (double d:sigma_bMin)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("sigma_directedMin");
		for (double d:sigma_directedMin)
			str = str.concat(" "+d);		
		str = str.concat("\n");
		str = str.concat("sigma_directedMax");
		for (double d:sigma_directedMax)
			str = str.concat(" "+d);		
		str = str.concat("\n");
		str = str.concat("vMin");
		for (double d:vMin)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("vMax");
		for (double d:vMax)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("p_db");
		for (double d:p_db)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("p_bd");
		for (double d:p_bd)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("iMin");
		for (double d:iMin)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("iMax");
		for (double d:iMax)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("\n");
				
		// Poisson noise settings
		str = str.concat("PoissonNoise "+PoissonNoise+"\n");
		str = str.concat("backgroundPoisson "+backgroundPoisson+"\n");
		str = str.concat("gain "+gain+"\n");
		str = str.concat("\n");
		
		// Gaussian noise settings
		str = str.concat("GaussianNoise "+GaussianNoise+"\n");
		str = str.concat("meanGaussian "+meanGaussian+"\n");
		str = str.concat("stdGaussian "+stdGaussian+"\n");
		return str;
	}
	
	public Bench2DConfiguration(){}

	public Bench2DConfiguration(String str) throws Exception
	{
		int cnt = 0;
		String[] tab = str.split("\n");
		for (String s:tab)
		{
			cnt++;
			try{
				if (s.startsWith("numBenchs"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setNumBenchs(Integer.parseInt(tab2[1]));
				}
				else if (s.startsWith("saveDir"))
				{
					//String[] tab2 = s.split("\'");
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setSaveDir(tab2[1]);
				}
				else if (s.startsWith("saveSequences"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setSaveSequences(Boolean.parseBoolean(tab2[1]));
				}
				else if (s.startsWith("displaySequences"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setDisplaySequences(Boolean.parseBoolean(tab2[1]));
				}
				else if (s.startsWith("seqLength"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setSeqLength(Integer.parseInt(tab2[1]));
				}
				else if (s.startsWith("imageType"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						try {setDataType(stringTodataType(tab2[1]));}
						catch (Exception e) {
							throw e;
						}
					}
				}
				else if (s.startsWith("width"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setWidth(Integer.parseInt(tab2[1]));
				}
				else if (s.startsWith("height"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setHeight(Integer.parseInt(tab2[1]));
				}
				else if (s.startsWith("numParticleGroups"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setNumParticleGroups(Integer.parseInt(tab2[1]));
				}
				else if (s.startsWith("numInitParticles"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						int[] numInitParticles = new int[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							numInitParticles[i-1] = Integer.parseInt(tab2[i]);
						setNumInitParticles(numInitParticles);
					}
				}
				else if (s.startsWith("numNewParticlesPerFrame"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] numNewParticlesPerFrame = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							numNewParticlesPerFrame[i-1] = Double.parseDouble(tab2[i]);
						setNumNewParticlesPerFrame(numNewParticlesPerFrame);
					}
				}
				else if (s.startsWith("pDisappear"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						setpDisappear(Double.parseDouble(tab2[1]));
					}
				}
				else if (s.startsWith("sigma_bMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] sigma_bMax = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_bMax[i-1] = Double.parseDouble(tab2[i]);
						setSigma_bMax(sigma_bMax);
					}
				}
				else if (s.startsWith("sigma_bMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] sigma_bMin = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_bMin[i-1] = Double.parseDouble(tab2[i]);
						setSigma_bMin(sigma_bMin);
					}
				}
				else if(s.startsWith("sigma_directedMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] sigma_dMin = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_dMin[i-1] = Double.parseDouble(tab2[i]);
						setSigma_directedMin(sigma_dMin);
					}
				}
				else if(s.startsWith("sigma_directedMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] sigma_dMax = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_dMax[i-1] = Double.parseDouble(tab2[i]);
						setSigma_directedMax(sigma_dMax);
					}
				}
				else if (s.startsWith("vMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] v = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							v[i-1] = Double.parseDouble(tab2[i]);
						setVMin(v);
					}
				}
				else if (s.startsWith("vMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] v = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							v[i-1] = Double.parseDouble(tab2[i]);
						setVMax(v);
					}
				}
				else if (s.startsWith("p_bd"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] p_bd = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							p_bd[i-1] = Double.parseDouble(tab2[i]);
						setP_bd(p_bd);
					}
				}
				else if (s.startsWith("p_db"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] p_db = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							p_db[i-1] = Double.parseDouble(tab2[i]);
						setP_db(p_db);
					}
				}
				else if (s.startsWith("iMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] iMin = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							iMin[i-1] = Double.parseDouble(tab2[i]);
						setiMin(iMin);
					}
				}
				else if (s.startsWith("iMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						double[] iMax = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							iMax[i-1] = Double.parseDouble(tab2[i]);
						setiMax(iMax);
					}
				}
				else if (s.startsWith("meanGaussian"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setMeanGaussian(Double.parseDouble(tab2[1]));
				}
				else if (s.startsWith("stdGaussian"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setStdGaussian(Double.parseDouble(tab2[1]));
				}
				else if (s.startsWith("gain"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setGain(Double.parseDouble(tab2[1]));
				}
				else if (s.startsWith("backgroundPoisson"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setBackgroundPoisson(Double.parseDouble(tab2[1]));
				}
				else if (s.startsWith("PoissonNoise"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setPoissonNoise(Boolean.parseBoolean(tab2[1]));
				}
				else if (s.startsWith("GaussianNoise"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						setGaussianNoise(Boolean.parseBoolean(tab2[1]));
				}
			}
			catch (Exception e)
			{
				if (e instanceof IllegalArgumentException)
					throw new IllegalArgumentException("Error while reading line "+cnt + ": " + e.getMessage());
				else throw (e);
			}
		}
	}
	
	public String dataTypeToString(int intType) throws IllegalArgumentException
	{
		String str = TypeUtil.toString(intType);
		if (str.compareToIgnoreCase("undefined")==0)
			throw(new IllegalArgumentException("Unkwnown image Type. Use a format defined in TypeUtil"));
		return str;
	}
	
	public int stringTodataType(String str) throws IllegalArgumentException
	{
		int dataType = TypeUtil.getDataType(str);
		if (dataType == TypeUtil.TYPE_UNDEFINED)
			throw(new IllegalArgumentException("Unkwnown image Type. Use a format defined in TypeUtil"));			
		return dataType;
	}
}
