package plugins.nchenouard.particleTracking.sequenceGenerator;

public class GaussProfile2D implements Profile2D
{
	private final double a;
	private final double varx;
	private final double vary;

	public GaussProfile2D(double a1, double sigx12, double sigy12)
	{
		a = a1;
		varx = sigx12;
		vary = sigy12;
	}
	public double get_a()
	{
		return a;
	}

	public double getVarx()
	{
		return varx;
	}
	public double getVary()
	{
		return vary;
	}
	public double getValue(double x0, double y0, double x, double y)
	{	
		return a * Math.exp(-((x-x0)*(x-x0)/varx + (y-y0)*(y-y0)/vary)/2);
	}

	public Object clone() throws CloneNotSupportedException
	{
		return new GaussProfile2D(a, varx, vary);
	}

	public static double getValue(double a, double varx, double vary, double x0, double y0, double x, double y)
	{	
		return a * Math.exp(-((x-x0)*(x-x0)/varx + (y-y0)*(y-y0)/vary)/2);
	}
	public double getValue(double[] params) {
		return a * Math.exp(-((params[2]-params[0])*(params[2]-params[0])/varx + (params[3]-params[1])*(params[3]-params[1])/vary)/2);
	}
}
