package plugins.nchenouard.particleTracking.sequenceGenerator;

public class PSF_2D implements Profile2D
{
	/**
	 * 
	 */
	private final double[] val;
	private final int maxIdx;
	
	private final double scale;//1 for 100nm
	private final double tabScale;//1 for 100nm
	private final double a;
	
	public PSF_2D(double a, double scale, double tabScale, double[] val)
	{
		this.a = a;
		this.scale = scale;
		this.tabScale = tabScale;
		this.val = val;
		this.maxIdx = val.length -1;
	}
	
	public double getValue(double x0, double y0, double x, double y) {
		double dist = Math.sqrt((x0-x)*(x0-x) + (y0-y)*(y0-y))*scale*tabScale;
		int idx = (int)Math.min(maxIdx, Math.round(dist));
		return a*val[idx];
	}
	
	public double getScaledValue(double x0, double y0, double x, double y) {
		double dist = Math.sqrt((x0-x)*(x0-x) + (y0-y)*(y0-y))*tabScale;
		int idx = (int)Math.min(maxIdx, Math.round(dist));
		return a*val[idx];
	}

	public double getValue(double[] params) {
		double dist = Math.sqrt((params[2]-params[0])*(params[2]-params[0]) + (params[3]-params[1])*(params[3]-params[1]))*scale*tabScale;
		int idx = (int)Math.min(maxIdx, Math.round(dist));
		return a*val[idx];
	}
	
	public Object clone() throws CloneNotSupportedException
	{
		return new PSF_2D(a, scale, tabScale, val);
	}
	
	
	public static PSF_2D autoLoadDefocalizedPSF_WFNA1(double a, double scalexy, double scalez, double z)
	{
		PSF_3D psf3D = PSF_3D.autoLoadPSF_WFNA1(a, scalexy, scalez);
		double[] val = psf3D.extractPSF2D(z);
		return new PSF_2D(a, scalexy, psf3D.tabScalexy, val);
	}
	
	public static PSF_2D autoLoadDefocalizedPSF_WFNA05(double a, double scalexy, double scalez, double z)
	{
		PSF_3D psf3D = PSF_3D.autoLoadPSF_WFNA05(a, scalexy, scalez);
		double[] val=psf3D.extractPSF2D(z);
		return new PSF_2D(a, scalexy, psf3D.tabScalexy, val);
	}
	
	public static PSF_2D autoLoadDefocalizedPSF_CLSMNA1(double a, double scalexy, double scalez, double z)
	{
		PSF_3D psf3D = PSF_3D.autoLoadPSF_CLSMNA1(a, scalexy, scalez);
		double[] val = psf3D.extractPSF2D(z);
		return new PSF_2D(a, scalexy, psf3D.tabScalexy, val);
	}
	
	public static PSF_2D autoLoadDefocalizedPSF_CLSMNA05(double a, double scalexy, double scalez, double z)
	{
		PSF_3D psf3D = PSF_3D.autoLoadPSF_CLSMNA05(a, scalexy, scalez);
		double[] val = psf3D.extractPSF2D(z);
		return new PSF_2D(a, scalexy, psf3D.tabScalexy, val);
	}
}
