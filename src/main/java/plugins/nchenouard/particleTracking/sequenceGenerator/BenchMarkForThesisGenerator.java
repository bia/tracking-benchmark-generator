package plugins.nchenouard.particleTracking.sequenceGenerator;

import icy.file.Saver;
import icy.gui.frame.progress.ProgressFrame;
import icy.sequence.Sequence;

import java.io.File;
import java.util.ArrayList;


import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;
import plugins.fab.trackmanager.processors.TrackProcessorExportTrackToXLS;
import plugins.nchenouard.particleTracking.sequenceGenerator.TrackGeneratorWithProfiles.BrownianTarget;
import plugins.nchenouard.particleTracking.sequenceGenerator.TrackGeneratorWithProfiles.SwitchingMotionTypeTarget;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Point3D;

public class BenchMarkForThesisGenerator
{
	int minTrackLength = 4;

	public void generate2DBenchMark(Bench2DConfiguration config, ArrayList<Sequence> seqList, ArrayList<TrackGroup> tgList) throws Exception
	{
		if (config.isSaveSequences() && config.getSaveDir().length()>0)
		{
			boolean success=true;
			if (!(new File(config.getSaveDir()).exists()))
				success =(new File(config.getSaveDir()).mkdir()) && success;
			if (success)
			{
				if (!(new File(config.getSaveDir()+"/images")).exists())
					success =(new File(config.getSaveDir()+"/images")).mkdir() && success;
				if (!(new File(config.getSaveDir()+"/tracks")).exists())
					success =(new File(config.getSaveDir()+"/tracks")).mkdir() && success;
			}
			if (!success)
			{
				throw new Exception("Error when trying to set the save directories");
			}
		}
		int[] dims =  new int[]{config.getWidth(), config.getHeight(), 1};
		TrackGeneratorWithProfiles tgp = new TrackGeneratorWithProfiles(2, dims, 1);

		ProgressFrame bar = new ProgressFrame("");
		bar.setLength(config.getNumBenchs());
		for (int cnt = 0; cnt < config.getNumBenchs(); cnt++)
		{	
			bar.setPosition(cnt+1);
			bar.setMessage("Generating tracks "+(cnt)+" over " + config.getNumBenchs());
			ArrayList<TrackSegment> tracks = new ArrayList<TrackSegment>();
			ArrayList<Profile> profiles = new ArrayList<Profile>();
			for (int g = 0; g < config.getNumParticleGroups(); g++)
			{
				int numParticles;
				if (config.getNumInitParticles().length>g)
					numParticles = config.getNumInitParticles()[g];
				else
					numParticles = config.getNumInitParticles()[0];
				double iMin;
				if (config.getiMin().length > g)
					iMin = config.getiMin()[g];
				else
					iMin = config.getiMin()[0];
				double iMax;
				if (config.getiMax().length > g)
					iMax = config.getiMax()[g];
				else
					iMax = config.getiMax()[0];
				double sMin;
				if (config.getSigma_bMin().length > g)
					sMin = config.getSigma_bMin()[g];
				else
					sMin = config.getSigma_bMin()[0];
				double sMax;
				if (config.getSigma_bMax().length > g)
					sMax = config.getSigma_bMax()[g];
				else
					sMax = config.getSigma_bMax()[0];
				double p_db;
				if (config.getP_db().length > g)
					p_db = config.getP_db()[g];
				else
					p_db = config.getP_db()[0];
				double p_bd;
				if (config.getP_bd().length > g)
					p_bd = config.getP_bd()[g];
				else
					p_bd = config.getP_bd()[0];
				double vMin, vMax;
				if (config.getVMin().length>g)
					vMin = config.getVMin()[g]; 
				else
					vMin = config.getVMin()[0];
				if (config.getVMax().length>g)
					vMax = config.getVMax()[g]; 
				else
					vMax = config.getVMax()[0];
				double sdMin, sdMax;
				if (config.getSigma_directedMin().length>g)
					sdMin = config.getSigma_directedMin()[g];
				else
					sdMin = config.getSigma_directedMax()[0];
				if (config.getSigma_directedMax().length>g)
					sdMax = config.getSigma_directedMax()[g];
				else
					sdMax = config.getSigma_directedMax()[0];
			
				//	create profiles
				ArrayList<Profile> tmpProfiles = TrackGeneratorWithProfiles.createCLSM05Profiles_2d(numParticles, iMin, iMax, 0, 0);
				//then create initial tracks
				ArrayList<TrackSegment> tmpTsList = createSwitchingMotionTracks(tgp, tmpProfiles, 0, config.getSeqLength()-1, dims, 2,  new int[]{0, 0, 0}, sMin, sMax, sdMin, sdMax, vMin, vMax, p_db, p_bd, config.getpDisappear());
				//check the lenght of tracks
				for (int i =0; i < tmpProfiles.size(); i++)
				{
					TrackSegment ts = tmpTsList.get(i);
					if (ts.getDetectionList().size()>=minTrackLength)
					{
						tracks.add(ts);
						profiles.add(tmpProfiles.get(i));
					}
				}
				//then create appearing tracks
				for (int i = 1; i < config.getSeqLength();i++)
				{
					double numNew;
					if (config.getNumNewParticlesPerFrame().length > g)
						numNew = config.getNumNewParticlesPerFrame()[g];
					else
						numNew = config.getNumNewParticlesPerFrame()[0];	
					int numInt = ((int)numNew);
					if (Math.random()< (numNew-(double)numInt))
						numInt =  numInt + 1;
					if (numInt>=1)
					{
						ArrayList<Profile> newProfiles = TrackGeneratorWithProfiles.createCLSM05Profiles_2d(numInt, iMin, iMax, 0, 0);
						ArrayList<TrackSegment> newTracks = createSwitchingMotionTracks(tgp, newProfiles, i, config.getSeqLength()-1, dims, 2,  new int[]{0, 0, 0}, sMin, sMax, sdMin, sdMax, vMin, vMax, p_db, p_bd, config.getpDisappear());
						for (int j =0; j < newProfiles.size(); j++)
						{
							TrackSegment ts = newTracks.get(j);
							if (ts.getDetectionList().size()>=minTrackLength)
							{
								tracks.add(ts);
								profiles.add(newProfiles.get(j));
							}
						}
					}
				}
			}
			bar.setMessage("Generating sequence "+(cnt+1)+" over " + config.getNumBenchs());
			boolean PoissonNoise = config.isPoissonNoise();
			boolean GaussianNoise = config.isGaussianNoise();
			Sequence seq=null;
			if (PoissonNoise && GaussianNoise)
				seq = TrackGeneratorWithProfiles.createTracksImagePoissonAndGaussian(tracks, profiles, config.getSeqLength(), dims, 2, 1, config.getMeanGaussian(), config.getStdGaussian(), config.getGain(), config.getBackgroundPoisson(), config.getDataType(), null);
			else
			{
				if (PoissonNoise)
				{
					seq = TrackGeneratorWithProfiles.createTracksImagePoisson(tracks, profiles, config.getSeqLength(), dims, 2, 1, config.getGain(), config.getBackgroundPoisson(), config.getDataType(), null);
				}
				if (GaussianNoise)
				{
					seq = TrackGeneratorWithProfiles.createTracksImageGaussian(tracks, profiles, config.getSeqLength(), dims, 2, 1, config.getMeanGaussian(), config.getStdGaussian(), config.getDataType(), null);
				}
			}
			if (seq==null)
			{
				seq = TrackGeneratorWithProfiles.createTracksImage(tracks, profiles, config.getSeqLength(), dims, 2, 1, config.getDataType(), null);
			}
			seq.setName("bench"+cnt);
			TrackGroup refTrackGroup = new TrackGroup(seq);
			refTrackGroup.setDescription("refTracks"+cnt);
			for (TrackSegment ts:tracks)
				refTrackGroup.addTrackSegment(ts);
			tracks = cutExitingTracks(tracks, config.getWidth(), config.getHeight(), 1);
			if (config.isDisplaySequences())
			{
				seqList.add(seq);
				tgList.add(refTrackGroup);
			}
			if (config.isSaveSequences())
			{
			    // export sequence
				File file = new File(config.getSaveDir()+"/images/bench"+cnt+".tif");
				Saver.save(seq, file, false, true);
				// export tracks
                File file2 = new File(config.getSaveDir()+"/tracks/bench"+cnt+".xls");
                TrackProcessorExportTrackToXLS.exportToXLS(refTrackGroup.getTrackSegmentList(), file2);
			}			
		}
		bar.close();
	}

	public void generate3DBenchMark(Bench3DConfiguration config, ArrayList<Sequence> seqList, ArrayList<TrackGroup> tgList) throws Exception
	{
		if (config.isSaveSequences() && config.getSaveDir().length()>0)
		{
			boolean success=true;
			if (!(new File(config.getSaveDir()).exists()))
				success =(new File(config.getSaveDir())).mkdir() && success;
			if (!(new File(config.getSaveDir()+"/images")).exists())
				success =(new File(config.getSaveDir()+"/images")).mkdir() && success;
			if (!(new File(config.getSaveDir()+"/tracks")).exists())
				success =(new File(config.getSaveDir()+"/tracks")).mkdir() && success;
			if (!success)
			{
				throw new Exception("Error when trying to set the save directories");
			}
		}
		int[] dims =  new int[]{config.getWidth(), config.getHeight(), config.getDepth()};
		TrackGeneratorWithProfiles tgp = new TrackGeneratorWithProfiles(3, dims, 1);

		ProgressFrame bar = new ProgressFrame("");
		bar.setLength(config.getNumBenchs());
		for (int cnt = 0; cnt < config.getNumBenchs(); cnt++)
		{	
			bar.setPosition(cnt);
			bar.setMessage("Generating tracks "+(cnt+1)+" over " + config.getNumBenchs());
			ArrayList<TrackSegment> tracks = new ArrayList<TrackSegment>();
			ArrayList<Profile> profiles = new ArrayList<Profile>();
			for (int g = 0; g < config.getNumParticleGroups(); g++)
			{
				int numParticles;
				if (config.getNumInitParticles().length>g)
					numParticles = config.getNumInitParticles()[g];
				else
					numParticles = config.getNumInitParticles()[0];
				double iMin;
				if (config.getiMin().length > g)
					iMin = config.getiMin()[g];
				else
					iMin = config.getiMin()[0];
				double iMax;
				if (config.getiMax().length > g)
					iMax = config.getiMax()[g];
				else
					iMax = config.getiMax()[0];
				double sMin;
				if (config.getSigma_bMin().length > g)
					sMin = config.getSigma_bMin()[g];
				else
					sMin = config.getSigma_bMin()[0];
				double sMax;
				if (config.getSigma_bMax().length > g)
					sMax = config.getSigma_bMax()[g];
				else
					sMax = config.getSigma_bMax()[0];
				double p_db;
				if (config.getP_db().length > g)
					p_db = config.getP_db()[g];
				else
					p_db = config.getP_db()[0];
				double p_bd;
				if (config.getP_bd().length > g)
					p_bd = config.getP_bd()[g];
				else
					p_bd = config.getP_bd()[0];
				double vMin, vMax;
				if (config.getVMin().length>g)
					vMin = config.getVMin()[g]; 
				else
					vMin = config.getVMin()[0];
				if (config.getVMax().length>g)
					vMax = config.getVMax()[g]; 
				else
					vMax = config.getVMax()[0];
				double sdMin, sdMax;
				if (config.getSigma_directedMin().length>g)
					sdMin = config.getSigma_directedMin()[g];
				else
					sdMin = config.getSigma_directedMax()[0];
				if (config.getSigma_directedMax().length>g)
					sdMax = config.getSigma_directedMax()[g];
				else
					sdMax = config.getSigma_directedMax()[0];
				
				//	create profiles
				ArrayList<Profile> tmpProfiles = TrackGeneratorWithProfiles.createCLSM05Profiles_3d(numParticles, iMin, iMax, config.getScaleZ());

				//then create tracks
				tracks.addAll(createSwitchingMotionTracks(tgp, tmpProfiles, 0, config.getSeqLength()-1, dims, 3,  new int[]{0, 0, 0}, sMin, sMax, sdMin, sdMax, vMin, vMax, p_db, p_bd, config.getpDisappear()));		
				profiles.addAll(tmpProfiles);
			}
			bar.setMessage("Generating sequence "+(cnt+1)+" over " + config.getNumBenchs());
			boolean PoissonNoise = config.isPoissonNoise();
			boolean GaussianNoise = config.isGaussianNoise();
			Sequence seq=null;
			if (PoissonNoise && GaussianNoise)
				seq = TrackGeneratorWithProfiles.createTracksImagePoissonAndGaussian(tracks, profiles, config.getSeqLength(), dims, 3, config.getScaleZ(), config.getMeanGaussian(), config.getStdGaussian(), config.getGain(), config.getBackgroundPoisson(), config.getDataType(), null);
			else
			{
				if (PoissonNoise)
				{
					seq = TrackGeneratorWithProfiles.createTracksImagePoisson(tracks, profiles, config.getSeqLength(), dims, 3, config.getScaleZ(), config.getGain(), config.getBackgroundPoisson(), config.getDataType(), null);
				}
				if (GaussianNoise)
				{
					seq = TrackGeneratorWithProfiles.createTracksImageGaussian(tracks, profiles, config.getSeqLength(), dims, 3, config.getScaleZ(), config.getMeanGaussian(), config.getStdGaussian(), config.getDataType(), null);
				}
			}
			if (seq==null)
			{
				seq = TrackGeneratorWithProfiles.createTracksImage(tracks, profiles, config.getSeqLength(), dims, 3, config.getScaleZ(), config.getDataType(), null);
			}
			seq.setName("bench"+cnt);
			TrackGroup refTrackGroup = new TrackGroup(seq);
			refTrackGroup.setDescription("refTracks"+cnt);
			for (TrackSegment ts:tracks)
				refTrackGroup.addTrackSegment(ts);
			if (config.isDisplaySequences())
			{
				seqList.add(seq);
				tgList.add(refTrackGroup);
			}
			if (config.isSaveSequences())
			{
				File file = new File(config.getSaveDir()+"/images/bench"+cnt+".tif");
                Saver.save(seq, file, false, true);
                File file2 = new File(config.getSaveDir()+"/tracks/bench"+cnt+".xls");
                TrackProcessorExportTrackToXLS.exportToXLS(refTrackGroup.getTrackSegmentList(), file2);
			}
		}
		bar.close();
		return;
	}

	public ArrayList<TrackSegment> createSwitchingMotionTracks(TrackGeneratorWithProfiles tgp, ArrayList<Profile> profiles, int firstT, int lastT, int[] dims, int dim, int [] gaps, double sMin, double sMax, double sdMin, double sdMax, double vMin, double vMax, double p_db, double p_bd, double pDisappear)
	{
		ArrayList<TrackSegment> tracks = new ArrayList<TrackSegment>();

		int i=0;
		for (Profile profile:profiles)
		{
			double s  = sMin + Math.random()*(sMax-sMin);
			double v  = vMin + Math.random()*(vMax-vMin);			
			double sd = sdMin + Math.random()*(sdMax-sdMin);
			TrackSegment ts;
			if (dim == 2)
			{	
				double x = (double)gaps[0] + Math.random()*(dims[0]-2*gaps[0]);
				double y = (double)gaps[1] + Math.random()*(dims[1]-2*gaps[1]);
				SwitchingMotionTypeTarget target = tgp.createSwitchingMotionTypeTarget(profile, new Point3D(x, y, 0), s, sd, v, dim, p_db, p_bd, pDisappear);
				ts = TrackGeneratorWithProfiles.createTrack(target, firstT, lastT);
			}
			else
			{
				double x = (double)gaps[0] + Math.random()*(dims[0]-2*gaps[0]);
				double y = (double)gaps[1] + Math.random()*(dims[1]-2*gaps[1]);
				double z = (double)gaps[2] + Math.random()*(dims[2]-2*gaps[2]);
				SwitchingMotionTypeTarget target = tgp.createSwitchingMotionTypeTarget(profile, new Point3D(x, y, z), s, sd, v, dim, p_db, p_bd, pDisappear);
				ts = TrackGeneratorWithProfiles.createTrack(target, firstT, lastT);
			}

			if (ts!=null)
				tracks.add(ts);
			i++;
		}
		return tracks;
	}

	public ArrayList<TrackSegment> createBrownianTracks(TrackGeneratorWithProfiles tgp, ArrayList<Profile> profiles, int firstT, int lastT, int[] dims, int dim, int [] gaps, double[] s, String title)
	{
		ArrayList<TrackSegment> tracks = new ArrayList<TrackSegment>();

		int i=0;
		for (Profile profile:profiles)
		{
			TrackSegment ts = createRandomBrownianTrack(tgp, profile, firstT, lastT, dims, gaps, s[i], dim);
			if (ts!=null)
				tracks.add(ts);
			i++;
		}
		return tracks;
	}

	public TrackSegment createRandomBrownianTrack(TrackGeneratorWithProfiles tgp, Profile profile, int firstT, int lastT, int[] dims, int[] gaps, double s, int dim)
	{
		if (dim == 2)
		{	
			double x = (double)gaps[0] + Math.random()*(dims[0]-2*gaps[0]);
			double y = (double)gaps[1] + Math.random()*(dims[1]-2*gaps[1]);
			BrownianTarget target = tgp.createBrownianTarget(profile, new Point3D(x, y, 0), s, dim);
			return TrackGeneratorWithProfiles.createTrack(target, firstT, lastT);
		}
		else
		{
			double x = (double)gaps[0] + Math.random()*(dims[0]-2*gaps[0]);
			double y = (double)gaps[1] + Math.random()*(dims[1]-2*gaps[1]);
			double z = (double)gaps[2] + Math.random()*(dims[2]-2*gaps[2]);
			BrownianTarget target = tgp.createBrownianTarget(profile, new Point3D(x, y, z), s, dim);
			return TrackGeneratorWithProfiles.createTrack(target, firstT, lastT);
		}
	}

	public ArrayList<TrackSegment> cutExitingTracks(ArrayList<TrackSegment> tsList, int width, int height, int depth)
	{
		if (true)//TODO: deprecated method
			return tsList;
	ArrayList<TrackSegment> tsList2 = new ArrayList<TrackSegment>();
	int numMaxOutImage = 2;
	for (TrackSegment ts:tsList)
	{
		int numPrevOutOfImage = 0;
		TrackSegment tmpTs = new TrackSegment();
		ArrayList<Detection> outDet = new ArrayList<Detection>();
		for (Detection d:ts.getDetectionList())
		{
			if (d.getX()<0 || d.getX()>=width || d.getY()<0 || d.getY()>=height || d.getZ()<0 || d.getZ()>=depth)
			{
				numPrevOutOfImage++;
				outDet.add(d);
			}
			else
			{
				if (numPrevOutOfImage==0)
				{
					tmpTs.addDetection(d);
				}
				else
				{
					if (numPrevOutOfImage>numMaxOutImage)
					{
						if (tmpTs.getDetectionList().size()>1)
						{
							ts.getOwnerTrackGroup().addTrackSegment(tmpTs);
							tsList2.add(tmpTs);
						}
						tmpTs = new TrackSegment();
					}
					else
					{
						//add out of image detections
						if (!tmpTs.getDetectionList().isEmpty())
							for (Detection dOut:outDet)
								tmpTs.addDetection(dOut);
					}
					tmpTs.addDetection(d);
					outDet.clear();
					numPrevOutOfImage = 0;
				}
			}
		}
		if (tmpTs.getDetectionList().size()>1)
		{
			ts.getOwnerTrackGroup().addTrackSegment(tmpTs);
			tsList2.add(tmpTs);
		}
	}
	return tsList2;
	}
}
