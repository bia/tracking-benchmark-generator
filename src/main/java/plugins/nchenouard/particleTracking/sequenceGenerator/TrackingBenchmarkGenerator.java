package plugins.nchenouard.particleTracking.sequenceGenerator;

import icy.gui.frame.IcyFrame;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.frame.progress.ProgressFrame;
import icy.main.Icy;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginImageAnalysis;
import icy.sequence.Sequence;
import icy.swimmingPool.SwimmingObject;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackManager;
import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.DetectionResult;
import plugins.nchenouard.spot.Spot;

/**
 * Generates realistic image sequences of moving particles in 2D and 3D fluorescence microcopy
 * Facilities are offered for automatically displaying and/or saving the sequences together with the synthetic particle tracks and detections
 * Multiple configurations are easily run thanks to the automatic parsing of mutliple configuration files
 * 
 * read manGenerator.txt or manGenerator.html before using
 * 
 * @author Nicolas Chenouard
 */

public class TrackingBenchmarkGenerator extends Plugin implements PluginImageAnalysis, ActionListener
{	
	IcyFrame mainFrame;
	JPanel mainPane;
	JComboBox defaultBox;
	JButton runButton;
	JButton loadFileButton;
	JButton runFilesButton;
	JButton helpButton;
	JTextArea textArea;
	Dimension framePreferredSize = new Dimension(500, 600);
	Dimension scrollPreferredSize = new Dimension(450, 550);

	ArrayList<Thread> runningThreadsList = new ArrayList<Thread>();

	// default configuration for the 2D benchmark configuration
	Bench2DConfiguration default2DConfiguration = new Bench2DConfiguration();
	// default configuration for the 2D benchmark configuration
	Bench3DConfiguration default3DConfiguration = new Bench3DConfiguration();

	//___________________Model methods________________________________________
	@Override
	/**
	 * initialize the GUI and the default configurations for the benchmark
	 */
	public void compute()
	{
		mainFrame = new IcyFrame("Particle tracking benchmark generator", true, true, true, true);

		mainPane = new JPanel();
		mainPane.setLayout(new BorderLayout());
		mainFrame.getContentPane().add(mainPane);
		JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		mainPane.add(northPanel, BorderLayout.NORTH);

		defaultBox = new JComboBox(new String[]{"switch to default 2D", "switch to default 3D"});
		northPanel.add(defaultBox);
		defaultBox.setSelectedIndex(0);
		defaultBox.addActionListener(this);

		runButton = new JButton("run");
		runButton.addActionListener(this);
		northPanel.add(runButton);

		loadFileButton = new JButton("load file");
		loadFileButton.addActionListener(this);
		northPanel.add(loadFileButton);

		JPanel contentScrollPane = new JPanel();
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setColumns(60);
		textArea.setText(default2DConfiguration.toString());
		//		textArea.setPreferredSize(scrollPreferredSize);
		contentScrollPane.add(textArea);
		JScrollPane scrollPane = new JScrollPane(contentScrollPane);
		mainPane.add(scrollPane, BorderLayout.CENTER);

		runFilesButton = new JButton("run files");
		runFilesButton.addActionListener(this);
		northPanel.add(runFilesButton);

		helpButton = new JButton("help");
		helpButton.addActionListener(this);
		northPanel.add(helpButton);

		//mainFrame.setPreferredSize(framePreferredSize);
		scrollPane.setPreferredSize(scrollPreferredSize);
		mainFrame.pack();
		mainFrame.setVisible(true);
		mainFrame.addToMainDesktopPane();
		mainFrame.requestFocus();
	}
	//________________________________________________________________________

	//___________________Action Listener method_______________________________
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource()==defaultBox)
		{
			if (defaultBox.getSelectedIndex()==0)
				textArea.setText(default2DConfiguration.toString());
			if (defaultBox.getSelectedIndex()==1)
				textArea.setText(default3DConfiguration.toString());
		}
		else if (e.getSource()==runButton)
		{
			Thread thr = new Thread()
			{
				@Override
				public void run()
				{
					String text = textArea.getText(); // get the string to be parsed as a configuration file for the benchmark
					Object o = null;
					try
					{
						o = createConfigObject(text); // create the benchmark configuration from the parsed text
					}
					catch (Exception e)
					{
						AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences, invalid configuration: "+e.getMessage());
					}
					if (o!=null)
					{
						BenchMarkForThesisGenerator bg = new BenchMarkForThesisGenerator();
						if (o instanceof Bench2DConfiguration) // 2D+t sequences
						{
							Bench2DConfiguration config = (Bench2DConfiguration)o;
							ArrayList<Sequence> seqList = new ArrayList<Sequence>();
							ArrayList<TrackGroup> tgList = new ArrayList<TrackGroup>();
							try {
								bg.generate2DBenchMark(config, seqList, tgList);
							} catch (Exception e) {
								if (e instanceof IllegalArgumentException)
								{
									AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences: "+e.getMessage()); 
								}
								else
								{
									e.printStackTrace();
									AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences. Check the log."); 
								}
							}
							SwingUtilities.invokeLater(new SendTracksToPoolThr(seqList, tgList, true));
						}
						else if(o instanceof Bench3DConfiguration) // 3D+t sequences
						{
							Bench3DConfiguration config = (Bench3DConfiguration)o;
							ArrayList<Sequence> seqList = new ArrayList<Sequence>();
							ArrayList<TrackGroup> tgList = new ArrayList<TrackGroup>();
							try {
								bg.generate3DBenchMark(config, seqList, tgList);
							} catch (Exception e) {
								if (e instanceof IllegalArgumentException)
								{
									AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences: "+e.getMessage()); 
								}
								else
								{
									e.printStackTrace();
									AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences. Check the log."); 
								}
							}
							SwingUtilities.invokeLater(new SendTracksToPoolThr(seqList, tgList, true));
						}
					}
					runningThreadsList.remove(this);
				}
			};
			runningThreadsList.add(thr);
			thr.start();
		}
		else if (e.getSource() == loadFileButton) // load a configuration from a local text file
		{
			SwingUtilities.invokeLater(new Runnable()
			{
				public void run()
				{
					int returnValue = -1;
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
					if (mainFrame.isExternalized())
						returnValue = fileChooser.showDialog(mainFrame.getExternalFrame(), "Read configuration file");
					else
						returnValue = fileChooser.showDialog(mainFrame.getInternalFrame(), "Read configuration file");

					if (returnValue == JFileChooser.APPROVE_OPTION) 
					{
						String tracksFileName = (String)fileChooser.getSelectedFile().toString();
						File file = new File(tracksFileName);
						// copy the configuration file to the plugin textArea
						BufferedReader br;
						try {
							br = new BufferedReader(new FileReader(file));
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
							return;
						}
						textArea.setText("");
						try {
							String line=null;
							while((line=br.readLine())!=null)
							{
								textArea.append(line+"\n");
							}
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			});
		}
		else if (e.getSource() == runFilesButton) // execute multiple configuration files
		{
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					final JFileChooser fc = new JFileChooser();
					fc.setMultiSelectionEnabled(true);
					int returnVal = -1;
					if (mainFrame.isExternalized())
						returnVal = fc.showOpenDialog(mainFrame.getExternalFrame());
					else
						returnVal = fc.showOpenDialog(mainFrame.getInternalFrame());				
					if (returnVal == JFileChooser.APPROVE_OPTION)
					{
						final File[] files = fc.getSelectedFiles();
						Thread benchGeneratorThread = new Thread()
						{
							@Override
							public void run()
							{
								ProgressFrame bar = new ProgressFrame("");
								bar.setLength(files.length);
								for (int i =0; i < files.length; i++) // loop through the selected files and parse/execute the valid ones
								{	
									File file = files[i];
									bar.setMessage("Executing benchark "+file.getName());
									bar.setPosition(i);
									BufferedReader br;
									try {
										br = new BufferedReader(new FileReader(file));
									} catch (FileNotFoundException e1) {
										e1.printStackTrace();
										runningThreadsList.remove(this);
										bar.close();
										return;
									}
									String text = new String();
									try {
										String line=null;
										while((line=br.readLine())!=null)
										{
											text = text.concat(line+"\n");
										}
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									Object o = null;
									try
									{
										o = createConfigObject(text); // create the benchmark configuration from the parsed text
									}
									catch (Exception e)
									{
										AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences, invalid configuration "+file.getName()+": "+e.getMessage());
									}
									if (o!=null)
									{
										BenchMarkForThesisGenerator bg = new BenchMarkForThesisGenerator();
										if (o instanceof Bench2DConfiguration) // 2D+t sequences
										{
											Bench2DConfiguration config = (Bench2DConfiguration)o;
											ArrayList<Sequence> seqList = new ArrayList<Sequence>();
											ArrayList<TrackGroup> tgList = new ArrayList<TrackGroup>();
											try {
												bg.generate2DBenchMark(config, seqList, tgList);
												for (Sequence seq:seqList)
													//seq.setName("file"+i+"-"+seq.getName());
													seq.setName(file.getName()+"-"+seq.getName());
												for (TrackGroup tg:tgList)
													//tg.setDescription("file"+i+"-"+tg.getDescription());
													tg.setDescription(file.getName()+"-"+tg.getDescription());
											} catch (Exception e) {
												if (e instanceof IllegalArgumentException)
												{
													AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences for "+file.getName()+": "+e.getMessage()); 
												}
												else
												{
													e.printStackTrace();
													AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences for "+file.getName() +". Check the log."); 
												}
											}
											SwingUtilities.invokeLater(new SendTracksToPoolThr(seqList, tgList, true));
										}
										else if(o instanceof Bench3DConfiguration) // 3D+t sequences
										{
											Bench3DConfiguration config = (Bench3DConfiguration)o;
											ArrayList<Sequence> seqList = new ArrayList<Sequence>();
											ArrayList<TrackGroup> tgList = new ArrayList<TrackGroup>();
											try {
												bg.generate3DBenchMark(config, seqList, tgList);
												for (Sequence seq:seqList)
													//seq.setName("file"+i+"-"+seq.getName());
													seq.setName(file.getName()+"-"+seq.getName());
												for (TrackGroup tg:tgList)
													//tg.setDescription("file"+i+"-"+tg.getDescription());
													tg.setDescription(file.getName()+"-"+tg.getDescription());
											} catch (Exception e) {
												if (e instanceof IllegalArgumentException)
												{
													AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences for "+file.getName()+": "+e.getMessage()); 
												}
												else
												{
													e.printStackTrace();
													AnnounceFrame failFrame = new AnnounceFrame("Unable to create sequences for "+file.getName() +". Check the log."); 
												}
											}
											SwingUtilities.invokeLater(new SendTracksToPoolThr(seqList, tgList, true));
										}
									}
								}
								bar.setPosition(files.length);
								bar.setMessage("completed");
								bar.close();
								runningThreadsList.remove(this);
							}
						};
						runningThreadsList.add(benchGeneratorThread);
						benchGeneratorThread.start();
					}
				}
			});
		}
		else if (e.getSource()==helpButton)
		{
			SwingUtilities.invokeLater(new Runnable()
			{
				public void run()
				{
					InputStream stream = TrackingBenchmarkGenerator.class.getResourceAsStream("/plugins/nchenouard/particleTracking/sequenceGenerator/manGenerator.html");
					InputStreamReader streamReader= new InputStreamReader(stream);					
					BufferedReader br = new BufferedReader(streamReader);
				    String str = "";
					try {
						for(String line = br.readLine(); line!=null ; line = br.readLine())
						{
							str = str.concat(line+"\n");
						}
					} catch (IOException e) {
						e.printStackTrace();
						return;
					}
					JEditorPane htmlPane=null;
					htmlPane = new JEditorPane("text/html", str);
					
				    //File f = new File(stream);
					//String manFileName = "/plugins/nchenouard/particleTracking/sequenceGenerator/manGenerator.html";
					//File file = new File(manFileName);
//					URL manURL=null;
//						try {
//							manURL = file.toURI().toURL();
//						} catch (MalformedURLException e1) {
//							e1.printStackTrace();
//							return;
//						}
//					JEditorPane htmlPane=null;
//					try {
//						htmlPane = new JEditorPane(manURL);
//					} catch (IOException e) {
//						e.printStackTrace();
//						return;
//					}
					htmlPane.setEditable(false);
					//Put the editor pane in a scroll pane.
					JScrollPane editorScrollPane = new JScrollPane(htmlPane);
					editorScrollPane.setVerticalScrollBarPolicy(
					                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
					editorScrollPane.setPreferredSize(new Dimension(600, 500));
					//editorScrollPane.setMaximumSize(new Dimension(600, 800));
					
					IcyFrame manFrame = new IcyFrame("Benchmark generator help", true, true, true, true);
					manFrame.add(new JScrollPane(editorScrollPane));
					manFrame.pack();
					manFrame.setVisible(true);
					manFrame.addToMainDesktopPane();
					manFrame.requestFocus();
				}
			});
		}
	}

	private void sendDetectionsToPool(final TrackGroup trackGroup, final Sequence sequence) {
		SwingUtilities.invokeLater(new Runnable() // Create a new Runnable which contain the
		{
			public void run()
			{
				DetectionResult detections = new DetectionResult();
				for (TrackSegment ts:trackGroup.getTrackSegmentList())
					for (Detection detection:ts.getDetectionList())
						detections.addDetection(detection.getT(), new Spot(detection.getX(), detection.getY(), detection.getZ()));	

				detections.setSequence(sequence);
				SwimmingObject result = new SwimmingObject(detections);//should give the sequence from which detections have been generated	
				Icy.getMainInterface().getSwimmingPool().add( result );		
			}
		});

	}

	public Object createConfigObject(String txt) throws Exception
	{
		String[] tab = txt.split("\n");
		int dim = 0;
		for (String s:tab)
		{
			if (s.startsWith("dim"))
			{
				String[] tab2 = s.split(" ");
				if (tab2.length>1)
					dim = Integer.parseInt(tab2[1]);
			}
		}
		switch (dim)
		{
		case 2:
			return new Bench2DConfiguration(txt);
		case 3:
			return new Bench3DConfiguration(txt);
		default :
			throw new IllegalArgumentException("Indicate the dimension of the image in the file: dim 2 or dim 3");
		}
	}

	//	private void sendTracksToPool(final TrackGroup trackGroup,final Sequence sequence)
	//	{
	//		SwingUtilities.invokeLater(new Runnable() // Create a new Runnable which contain the
	//		{
	//			public void run()
	//			{
	//				SwimmingObject result = new SwimmingObject( trackGroup);//should give the sequence from which detections have been generated	
	//				Icy.getMainInterface().getSwimmingPool().add( result );
	//				//check if there is a track painter for this sequence
	////				for (Painter painter: sequence.getPainters())
	////				{
	////					if (painter instanceof SimpleTrackPainter && ((SimpleTrackPainter)painter).getSequence()==sequence)
	////						return;
	////				}
	////				sequence.addPainter(new SimpleTrackPainter(sequence));
	//			}
	//		});
	//	}

	// Runnable object that is adding sequences to Icy, sending trackgroups detections to the swimmingPool
	public class SendTracksToPoolThr implements Runnable
	{
		ArrayList<Sequence> seqList;
		ArrayList<TrackGroup> tgList;
		boolean exportDetections;

		public SendTracksToPoolThr(final ArrayList<Sequence> seqList, final ArrayList<TrackGroup> tgList, boolean exportDetections)
		{
			this.seqList = seqList;
			this.tgList = tgList;
			this.exportDetections = exportDetections;
		}

		@Override
		public void run() {
			TrackManager trackManager = new TrackManager();
			for (int i = 0; i<seqList.size(); i++)
			{
				addSequence(seqList.get(i));
				// export the tracks
				SwimmingObject result = new SwimmingObject(tgList.get(i));//should give the sequence from which detections have been generated
				Icy.getMainInterface().getSwimmingPool().add( result );
				// export the detections
				if (exportDetections)
					sendDetectionsToPool(tgList.get(i), seqList.get(i));
				trackManager.setDisplaySequence( seqList.get(i));				
			}
		}
	}
}