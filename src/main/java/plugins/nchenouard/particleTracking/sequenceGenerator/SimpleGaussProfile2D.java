package plugins.nchenouard.particleTracking.sequenceGenerator;

public class SimpleGaussProfile2D implements Profile2D
{
	private final double a;
	private final double varXY;

	public SimpleGaussProfile2D(double a, double varXY)
	{
		this.a = a;
		this.varXY = varXY;
	}

	public double get_a()
	{
		return a;
	}

	public double getVarXY()
	{
		return varXY;
	}

	public double getValue(double x0, double y0, double x, double y)
	{
		return a * Math.exp(-((x-x0)*(x-x0) + (y-y0)*(y-y0))/(2*varXY));
	}

	public Object clone() throws CloneNotSupportedException
	{
		return new SimpleGaussProfile2D(a, varXY);
	}

	public static double getValue(double a, double varXY, double x0, double y0, double x, double y)
	{	
		return a * Math.exp(-((x-x0)*(x-x0) + (y-y0)*(y-y0))/(2*varXY));
	}
	public double getValue(double[] params) {
		return a * Math.exp(-((params[2]-params[0])*(params[2]-params[0]) + (params[3]-params[1])*(params[3]-params[1]))/(2*varXY));
	}
}
