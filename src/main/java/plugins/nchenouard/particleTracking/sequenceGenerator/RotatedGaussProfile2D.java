package plugins.nchenouard.particleTracking.sequenceGenerator;

public class RotatedGaussProfile2D implements Profile2D
{
	private final double a;
	private final double varx;
	private final double vary;
	private final double theta;//rotation in radian
	private final double cosTheta;
	private final double sinTheta;
	
	public RotatedGaussProfile2D(double a, double varx, double vary, double theta)
	{
		this.a = a;
		this.varx = varx;
		this.vary = vary;
		this.theta=theta;
		cosTheta = Math.cos(theta);
		sinTheta = Math.sin(theta);
	}
	public double get_a()
	{
		return a;
	}

	public double getVarx()
	{
		return varx;
	}
	public double getVary()
	{
		return vary;
	}

	public double getTheta()
	{
		return theta;
	}

	public double getValue(double x0, double y0, double x, double y)
	{	
		double dx = (x-x0)*cosTheta - (y-y0)*sinTheta;
		double dy = (x-x0)*sinTheta + (y-y0)*cosTheta;
		return a * Math.exp(-(dx*dx/varx + dy*dy/vary)/2);
	}

	public Object clone() throws CloneNotSupportedException
	{
		return new RotatedGaussProfile2D(a, varx, vary, theta);
	}

	public static double getValue(double a, double varx, double vary, double theta, double x0, double y0, double x, double y)
	{	
		double dx = (x - x0)*Math.cos(theta) - (y-y0)*Math.sin(theta);
		double dy = (y - y0)*Math.cos(theta) + (x-x0)*Math.sin(theta);
		return a * Math.exp(-(dx*dx/varx + dy*dy/vary)/2);
	}
	/**
	 * @param params = {x0 y0 x x}
	 * {x0 y0} center of the Gaussian function
	 * {x y} point to evluate function
	 * */
	
	public double getValue(double[] params)
	{
		double dx = (params[2] - params[0])*Math.cos(theta) - (params[3]-params[1])*Math.sin(theta);
		double dy = (params[3] - params[1])*Math.cos(theta) + (params[2]-params[0])*Math.sin(theta);
		return a * Math.exp(-(dx*dx/varx + dy*dy/vary)/2);
	}
}
