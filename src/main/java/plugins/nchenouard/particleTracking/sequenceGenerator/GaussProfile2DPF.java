package plugins.nchenouard.particleTracking.sequenceGenerator;

public class GaussProfile2DPF implements Profile2D
{
	public GaussProfile2D expectedProfile;
	public final int numParticles;
	public GaussProfile2D[] randomProfiles;
	public double[] profilesWeight;
	
	public GaussProfile2DPF(GaussProfile2D initProfile, int numParticles)
	{
		expectedProfile = (GaussProfile2D) initProfile;
		this.numParticles = numParticles;
	}
	public double getValue(double x0, double y0, double x, double y)
	{	
//		double val = 0;
//		for (int i = 0; i < numParticles; i++)
//			val+=randomProfiles[i].getValue(x0, y0, x, y)*profilesWeight[i];
//		return val;
		return expectedProfile.getValue(x0, y0, x, y);
	}

	public Object clone() throws CloneNotSupportedException
	{
		GaussProfile2DPF newProfile = new GaussProfile2DPF((GaussProfile2D)this.expectedProfile.clone(), this.numParticles);
		newProfile.randomProfiles = new GaussProfile2D[numParticles];
		for (int i = 0; i < randomProfiles.length; i++)
			newProfile.randomProfiles[i] = (GaussProfile2D) randomProfiles[i].clone();
		newProfile.profilesWeight = new double[numParticles];
		System.arraycopy(profilesWeight, 0, newProfile.profilesWeight, 0, numParticles);
		return newProfile;
	}

	//expected value
	public double getValue(double[] params)
	{
		return expectedProfile.getValue(params[0], params[1], params[2], params[3]);
//		double x0 = params[0];
//		double y0 = params[1];
//		double x = params[2];
//		double y = params[3];
//		double val = 0;
//		for (int i = 0; i < numParticles; i++)
//			val+=randomProfiles[i].getValue(x0, y0, x, y)*profilesWeight[i];
//		return val;
	}
}
