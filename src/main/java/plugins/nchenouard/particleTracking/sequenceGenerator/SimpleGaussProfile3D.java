package plugins.nchenouard.particleTracking.sequenceGenerator;

public class SimpleGaussProfile3D implements Profile3D
{
	/**
	 * 
	 */
	final double a;
	final double varXY, varZ;

	public SimpleGaussProfile3D(double a, double varXY, double varZ)
	{
		this.a = a;
		this.varXY = varXY;
		this.varZ = varZ;
	}

	public double get_a(){return a;}
	public double getVarXY(){return varXY;}
	public double getVarZ(){return varZ;}

	/**
	 * @return value of profile at given position for specified center
	 * @param x : x position
	 * @param y : y position
	 * @param z : z position
	 * @param x0 : x coordinate of center
	 * @param y0 : y coordinate of center
	 * @param z0 : z coordinate of center
	 * **/
	public double getValue(double x0, double y0, double z0, double x, double y, double z) {
		return a * Math.exp(-(((x-x0)*(x-x0) + (y-y0)*(y-y0))/varXY + (z-z0)*(z-z0)/varZ)/2);
	}

	/**
	 * @return value of profile at given position for specified center
	 * @param params = {double x0, double y0, double z0, double x, double y, double z}
	 * **/
	public double getValue(double[] params) {
		return a * Math.exp(-(((params[3]-params[0])*(params[3]-params[0]) + (params[4]-params[1])*(params[4]-params[1]))/varXY + (params[5]-params[2])*(params[5]-params[2])/varZ)/2);
	}

	public static double getValue(double a, double varXY, double varZ, double x0, double y0, double z0, double x, double y, double z)
	{
		return a * Math.exp(-(((x-x0)*(x-x0) + (y-y0)*(y-y0))/varXY + (z-z0)*(z-z0)/varZ)/2);
	}

	public Object clone() throws CloneNotSupportedException
	{
		return new SimpleGaussProfile3D(a, varXY, varZ);
	}
}
