package plugins.nchenouard.particleTracking.sequenceGenerator;

import java.security.InvalidParameterException;

import icy.type.TypeUtil;

public class CopyOfBench3DConfiguration
{
	// benchmark configuration
	int numBenchs = 1;
	String saveDir = "";
	boolean displaySequences = true;
	boolean saveSequences = false;
	
	// sequence configuration
	final static int dim = 3;
	int seqLength = 25;
	int width = 256;
	int height = 256;
	int depth = 20;
	double scaleZ = 1.0;
	int dataType = TypeUtil.TYPE_DOUBLE;

	// particles configuration
	int numParticleGroups = 1;
	double[] numNewParticlesPerFrame = new double[]{1.0};
	int[] numInitParticles = new int[]{20};
	double pDisappear = 0.05;	
	double[] sigma_bMax = {4};
	double[] sigma_bMin = {2};
	double sigma_tethered = 1.0;
	double[] v = new double[]{2};
	double[] p_bd = new double[]{0.2};//switch from Brownian to directed motion
	double[] p_db = new double[]{0.2};//switch from directed to Brownian motion
	double[] iMin = new double[]{20};
	double[] iMax = new double[]{25};
	
	// Poisson noise settings
	boolean PoissonNoise = false;
	double backgroundPoisson = 50;
	double gain = 1;
	
	// Gaussian noise settings
	boolean GaussianNoise = true;
	double meanGaussian = 50;
	double stdGaussian = 5;


	public String toString()
	{
		String str = new String();
		
		// benchmark configuration
		str = str.concat("numBenchs "+numBenchs+"\n");
		str = str.concat("saveDir "+saveDir+"\n");
		str = str.concat("saveSequences "+saveSequences+"\n");
		str = str.concat("displaySequences "+displaySequences+"\n");
		str = str.concat("\n");
		
		// sequence configuration
		str = str.concat("dim "+dim+"\n");
		str = str.concat("seqLength "+seqLength+"\n");
		str = str.concat("width "+width+"\n");
		str = str.concat("height "+width+"\n");
		str = str.concat("depth "+depth+"\n");
		str = str.concat("scaleZ "+scaleZ+"\n");
		str = str.concat("imageType " + dataTypeToString(dataType)+"\n");
		str = str.concat("\n");
		
		// particles configuration
		str = str.concat("numParticleGroups "+numParticleGroups+"\n");
		str = str.concat("numInitParticles");
		for (int d:numInitParticles)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("numNewParticlesPerFrame");
		for (double d:numNewParticlesPerFrame)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("pDisappear "+pDisappear+"\n");
		str = str.concat("sigma_bMax");
		for (double d:sigma_bMax)
			str = str.concat(" "+d);
		str = str.concat("\n");	
		str = str.concat("sigma_bMin");
		for (double d:sigma_bMin)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("sigma_tethered "+sigma_tethered+"\n");
		str = str.concat("v");
		for (double d:v)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("p_db");
		for (double d:p_db)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("p_bd");
		for (double d:p_bd)
			str = str.concat(" "+d);
		str = str.concat("\n");
		
		str = str.concat("iMin");
		for (double d:iMin)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("iMax");
		for (double d:iMax)
			str = str.concat(" "+d);
		str = str.concat("\n");
		str = str.concat("\n");
		
		// Poisson noise settings
		str = str.concat("PoissonNoise "+PoissonNoise+"\n");
		str = str.concat("backgroundPoisson "+backgroundPoisson+"\n");
		str = str.concat("gain "+gain+"\n");
		str = str.concat("\n");
		
		// Gaussian noise settings
		str = str.concat("meanGaussian "+meanGaussian+"\n");
		str = str.concat("stdGaussian "+stdGaussian+"\n");
		return str;
	}


	public CopyOfBench3DConfiguration(){}

	public CopyOfBench3DConfiguration(String str)
	{
		int cnt = 0;
		String[] tab = str.split("\n");
		for (String s:tab)
		{
			cnt++;
			try{
				if (s.startsWith("numBenchs"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						numBenchs = Integer.parseInt(tab2[1]);
				}
				if (s.startsWith("saveDir"))
				{
					String[] tab2 = s.split("\'");
					if (tab2.length>1)
						saveDir = tab2[1];
				}
				if (s.startsWith("saveSequences"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						saveSequences = Boolean.parseBoolean(tab2[1]);
				}
				if (s.startsWith("displaySequences"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						displaySequences = Boolean.parseBoolean(tab2[1]);
				}
				else if (s.startsWith("seqLength"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						seqLength = Integer.parseInt(tab2[1]);
				}
				else if (s.startsWith("width"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						width = Integer.parseInt(tab2[1]);
				}
				else if (s.startsWith("height"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						height = Integer.parseInt(tab2[1]);
				}
				else if (s.startsWith("depth"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						depth = Integer.parseInt(tab2[1]);
				}
				else if (s.startsWith("scaleZ"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						scaleZ = Double.parseDouble(tab2[1]);
				}
				else if (s.startsWith("imageType"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						try {dataType = stringTodataType(tab2[1]);}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				else if (s.startsWith("numParticleGroups"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						numParticleGroups = Integer.parseInt(tab2[1]);
				}
				else if (s.startsWith("numInitParticles"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						numInitParticles = new int[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							numInitParticles[i-1] = Integer.parseInt(tab2[i]);
					}
				}
				else if (s.startsWith("numNewParticlesPerFrame"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						numNewParticlesPerFrame = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							numNewParticlesPerFrame[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("pDisappear"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						pDisappear = Double.parseDouble(tab2[1]);
					}
				}
				else if (s.startsWith("sigma_bMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						sigma_bMax = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_bMax[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("sigma_bMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						sigma_bMin = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							sigma_bMin[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if(s.startsWith("sigma_tethered"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						sigma_tethered = Double.parseDouble(tab2[1]);
				}
				else if (s.startsWith("v"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						v = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							v[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("p_bd"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						p_bd = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							p_bd[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("p_db"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						p_db = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							p_db[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("iMin"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						iMin = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							iMin[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("iMax"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
					{
						iMax = new double[tab2.length-1];
						for (int i=1; i < tab2.length;i++)
							iMax[i-1] = Double.parseDouble(tab2[i]);
					}
				}
				else if (s.startsWith("meanGaussian"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						meanGaussian = Double.parseDouble(tab2[1]);
				}
				else if (s.startsWith("stdGaussian"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						stdGaussian = Double.parseDouble(tab2[1]);
				}
				else if (s.startsWith("gain"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						gain = Double.parseDouble(tab2[1]);
				}
				else if (s.startsWith("backgroundPoisson"))
				{
					String[] tab2 = s.split(" ");
					if (tab2.length>1)
						backgroundPoisson = Double.parseDouble(tab2[1]);
				}
			}
			catch (Exception e)
			{
				System.err.println("Error while reading line "+cnt);
				e.printStackTrace();
			}
		}

	}
	public String dataTypeToString(int intType) throws InvalidParameterException
	{
		String str = TypeUtil.toString(intType);
		if (str.compareToIgnoreCase("undefined")==0)
			throw(new InvalidParameterException("Unkwnown image Type. Use a format defined in TypeUtil"));
		return str;
	}
	
	public int stringTodataType(String str) throws InvalidParameterException
	{
		int dataType = TypeUtil.getDataType(str);
		if (dataType == TypeUtil.TYPE_UNDEFINED)
			throw(new InvalidParameterException("Unkwnown image Type. Use a format defined in TypeUtil"));			
		return dataType;
	}
}
