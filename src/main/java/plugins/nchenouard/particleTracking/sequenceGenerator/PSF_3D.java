package plugins.nchenouard.particleTracking.sequenceGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PSF_3D implements Profile3D
{
	/**
	 * for scale = 1 : 
	 * 	dx = dy = 100nm
	 * suppose z is an symmetrty axis and XY is a symmetry plan
	 *
	 */

	private static final long serialVersionUID = 3363192864007419750L;
	public double tabScalexy;//1 for 100nm;
	public double tabScalez;//1 for 100nm
	private double scalexy;//1 for 100nm
	private double scalez;//1 for 100nm
	private double a;
	int maxIdxXY;
	int maxIdxZ;

	public final double[][] val;

	public PSF_3D(double a, double scalexy, double tabScalexy, double scalez, double tabScalez, double[][] val)
	{
		this.a = a;
		this.scalexy = scalexy;
		this.scalez = scalez;
		this.tabScalexy = tabScalexy;
		this.tabScalez = tabScalez;
		this.val = val;
		this.maxIdxZ = val.length-1;
		this.maxIdxXY = val[0].length-1;
	}

	public double[] extractPSF2D(double z)
	{
		int idx = (int)Math.round(z*scalez);
		int l = val[idx].length;
		double[] psf2d = new double[l];
		System.arraycopy(val[idx], 0, psf2d, 0, l);
		return psf2d;
	}

	public double getValue(double x0, double y0, double z0, double x, double y, double z) {
		double distXY = Math.sqrt((x0-x)*(x0-x) + (y0-y)*(y0-y))*scalexy*tabScalexy;
		int idxXY = (int)Math.min(maxIdxXY, Math.round(distXY));
		double distZ = Math.abs(z0-z)*scalez;
		int idxZ = (int)Math.min(maxIdxZ, Math.round(distZ));
		return a*val[idxZ][idxXY];
	}

	public double getValue(double[] params) {
		double distXY = Math.sqrt((params[3]-params[0])*(params[3]-params[0]) + (params[4]-params[1])*(params[4]-params[1]))*scalexy;
		int idxXY = (int)Math.min(maxIdxXY, Math.round(distXY));
		double distZ = Math.abs(params[5]-params[2])*scalez*tabScalez;
		int idxZ = (int)Math.min(maxIdxZ, Math.round(distZ));
		return a*val[idxZ][idxXY];
	}

	public Object clone() throws CloneNotSupportedException
	{	
		return new PSF_3D(a, scalexy, tabScalexy, scalez, tabScalez, val);
	}

	public static PSF_3D autoLoadPSF_WFNA1(double a, double scalexy, double scalez)
	{
		InputStream stream = PSF_3D.class.getResourceAsStream("/plugins/nchenouard/particleTracking/sequenceGenerator/PSFs/wfNA1_3D.dat");
		if (stream==null)
		{
			System.err.println("Error while loading psf file");
			return null;
		}
		//		URL url = PSF_3D.class.getResource("PSFs/wfNA1_3D.dat");
		//		if (url==null)
		//			return null;
		//		File f;
		//		try {
		//			f = new File(url.toURI());
		//		} catch (URISyntaxException e) {
		//			e.printStackTrace();
		//			return null;
		//		}
		return autoLoadPSF(a, scalexy,  0.25, scalez, 0.5, stream);
	}

	public static PSF_3D autoLoadPSF_WFNA05(double a, double scalexy, double scalez)
	{
		InputStream stream = PSF_3D.class.getResourceAsStream("/plugins/nchenouard/particleTracking/sequenceGenerator/PSFs/wfNA05_3D.dat");
		if (stream==null)
		{
			System.err.println("Error while loading psf file");
			return null;
		}
		return autoLoadPSF(a, scalexy, 1, scalez, 100/300, stream);
		//		URL url = PSF_3D.class.getResource("PSFs/wfNA05_3D.dat");
		//		if (url==null)
		//		{
		//			System.err.println("null psf url");
		//			return null;
		//		}
		//		File f;
		//		try {
		//			f = new File(url.toURI());
		//		} catch (URISyntaxException e) {
		//			e.printStackTrace();
		//			return null;
		//		}
		//		return autoLoadPSF(a, scalexy, 1, scalez, 100/300, f);
	}

	public static PSF_3D autoLoadPSF_CLSMNA1(double a, double scalexy, double scalez)
	{
		InputStream stream = PSF_3D.class.getResourceAsStream("/plugins/nchenouard/particleTracking/sequenceGenerator/PSFs/clsmNA1_3D.dat");
		if (stream==null)
		{
			System.err.println("Error while loading psf file");
			return null;
		}
		return autoLoadPSF(a, scalexy, 100/25, scalez, 100/50, stream);
	}

	public static PSF_3D autoLoadPSF_CLSMNA05(double a, double scalexy, double scalez)
	{
		InputStream stream = PSF_3D.class.getResourceAsStream("/plugins/nchenouard/particleTracking/sequenceGenerator/PSFs/clsmNA05_3D.dat");
		if (stream==null)
		{
			System.err.println("Error while loading psf file");
			return null;
		}
		return autoLoadPSF(a, scalexy, 1, scalez, 100/300, stream);
	}

	private static PSF_3D autoLoadPSF(double a, double scalexy, double tabScalexy, double scalez, double tabScalez, File f)
	{

		BufferedReader br;
		try {
			br = new BufferedReader(new java.io.FileReader(f));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		ArrayList<double[]> al = new ArrayList<double[]>();
		String[] splitted;
		int i;

		//read the file, break up and assign to short[]
		try {
			for(String line = br.readLine(); line!=null && line.length()>0 ; line = br.readLine())
			{

				splitted = line.split(",");
				double[] lineTab = new double[splitted.length];
				//convert strings to doubles
				for(i = 0; i<lineTab.length; i++)
					lineTab[i] = Double.parseDouble(splitted[i]);
				al.add(lineTab);
			}
		}
		catch (Exception e)
		{
			System.err.println("Error while loadind 3D PSF");
		}

		double[][] tab = new double[al.size()][];
		int cnt= 0;
		for(double[] line:al)
		{
			tab[cnt] = line;
			cnt++;
		}
		//all lines into the arraylist now
		try {
			br.close();
			br = null;
		} catch (IOException e){e.printStackTrace();}

		return new PSF_3D(a, scalexy, tabScalexy, scalez, tabScalez, tab);
	}

	private static PSF_3D autoLoadPSF(double a, double scalexy, double tabScalexy, double scalez, double tabScalez, InputStream stream)
	{
		InputStreamReader streamReader= new InputStreamReader(stream);
		BufferedReader br= new BufferedReader(streamReader);

		ArrayList<double[]> al = new ArrayList<double[]>();
		String[] splitted;
		int i;

		//read the file, break up and assign to short[]
		try {
			for(String line = br.readLine(); line!=null ; line = br.readLine())
			{
				if (line.length()>0)
				{
					splitted = line.split(",");
					double[] lineTab = new double[splitted.length];
					//convert strings to doubles
					for(i = 0; i<lineTab.length; i++)
						lineTab[i] = Double.parseDouble(splitted[i]);
					al.add(lineTab);
				}
			}
		}
		catch (Exception e)
		{
			System.err.println("Error while loadind 3D PSF");
		}

		double[][] tab = new double[al.size()][];
		int cnt= 0;
		for(double[] line:al)
		{
			tab[cnt] = line;
			cnt++;
		}
		//all lines into the arraylist now
		try {
			br.close();
			br = null;
		} catch (IOException e){e.printStackTrace();}

		return new PSF_3D(a, scalexy, tabScalexy, scalez, tabScalez, tab);
	}
}
