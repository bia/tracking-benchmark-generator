package plugins.nchenouard.particleTracking.sequenceGenerator;

public interface Profile2D extends Profile
{
	public double getValue(double x0, double y0, double x, double y);
}
