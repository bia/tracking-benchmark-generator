package plugins.nchenouard.particleTracking.sequenceGenerator;

public class GaussProfile3D implements Profile3D{
	final double a;
	final double varx, vary, varz;

	public GaussProfile3D(double a, double varx, double vary, double varz)
	{
		this.a = a;
		this.varx = varx;
		this.vary = vary;
		this.varz = varz;
	}

	public double get_a(){return a;}
	public double getVarx(){return varx;}
	public double getVary(){return vary;}
	public double getVarz(){return varz;}

	/**
	 * @return value of profile at given position for specified center
	 * @param x : x position
	 * @param y : y position
	 * @param z : z position
	 * @param x0 : x coordinate of center
	 * @param y0 : y coordinate of center
	 * @param z0 : z coordinate of center
	 * **/
	public double getValue(double x0, double y0, double z0, double x, double y, double z) {
		return a * Math.exp(-((x-x0)*(x-x0)/varx + (y-y0)*(y-y0)/vary + (z-z0)*(z-z0)/varz)/2);
	}

	/**
	 * @return value of profile at given position for specified center
	 * @param params = {double x0, double y0, double z0, double x, double y, double z}
	 * **/
	public double getValue(double[] params) {
		return a * Math.exp(-((params[3]-params[0])*(params[3]-params[0])/varx + (params[4]-params[1])*(params[4]-params[1])/vary + (params[5]-params[2])*(params[5]-params[2])/varz)/2);
	}

	public static double getValue(double a, double varx, double vary, double varz, double x0, double y0, double z0, double x, double y, double z)
	{
		return a * Math.exp(-((x-x0)*(x-x0)/varx + (y-y0)*(y-y0)/vary + (z-z0)*(z-z0)/varz)/2);
	}

	public Object clone() throws CloneNotSupportedException
	{
		return new GaussProfile3D(a, varx, vary, varz);
	}
}
