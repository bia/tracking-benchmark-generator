package plugins.nchenouard.particleTracking.sequenceGenerator;

/**
 * @author nicolas chenouard
 * */
public interface Profile {
		public double getValue(double[] params);
		public Object clone() throws CloneNotSupportedException;
}
