package plugins.nchenouard.particleTracking.sequenceGenerator;

public interface Profile3D extends Profile{
	public double getValue(double x0, double y0, double z0, double x, double y, double z);
}
